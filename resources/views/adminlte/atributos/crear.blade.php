@extends('layouts.adminlte.app')
@section('content')
	<form action="{{route('GuardarAtributoCategoriaAdmin',$actual->id)}}" method="POST">
		@csrf
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header with-border">
			          	<h3 class="box-title">
			          		Crear Atributo: <a href="{{route('ListarCategoriasAdmin',$actual->id)}}">{{$actual->nombre}}</a>
			          	</h3>

			          	<h3 class="box-title pull-right">
			          		Categoria: <a href="{{route('ListarCategoriasAdmin',$padre['id'])}}">{{$padre['nombre']}}</a>
			          	</h3>
			        </div>
			        <div class="box-body">
						<div class="form-group">
							<label for="nombre">Nombre</label>
							<input type="text" name="nombre" required class="form-control" placeholder="nombre">
						</div>
						<div class="form-group">
							<label for="nombre">Tipo</label>
							<select class="form-control" required name="tipo" onchange="cambiarContenido(value)">
								<option value="">Seleccionar</option>
								<option value="select">Lista</option>
								<option value="text">Texto</option>
								<option value="number">Numerico</option>
								<option value="number_list">Numerico Lista</option>
							</select>
						</div>

						<div class="form-group" id="contenido">

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
			        <div class="box-body">
						<div class="form-group">
							<center>
								<br>
								<input type="submit" value="Guardar" class="btn btn-info" placeholder="nombre">
							</center>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
		
@endsection

@section('javascript')
	<script type="text/javascript">
		var texto = '<input type="hidden" name="contenido">';
		var numerico = '<input type="hidden" name="contenido">';
		var defecto = '<input type="hidden" name="contenido">';
		var lista = 
			'<label for="nombre">Lista de Valores</label>'+
			'<textarea class="form-control" required name="contenido" placeholder="Separe por comas (,) cada elemento de la lista"></textarea>'
		;
		var numerico_lista = 
			'<label for="nombre">Lista de Valores</label>'+
			'<textarea class="form-control" required name="contenido" placeholder="Separe por comas (,) cada elemento de la lista"></textarea>'
		;

		function cambiarContenido(variable){
			switch (variable) {
				case 'select':
					document.getElementById('contenido').innerHTML = lista;
				break;
				case 'number_list':
					document.getElementById('contenido').innerHTML = numerico_lista;
				break;
				default:
					document.getElementById('contenido').innerHTML = defecto;
				break;
			}
		}
	</script>
@endsection

<?php
	
?>