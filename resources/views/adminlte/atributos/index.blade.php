@extends('layouts.adminlte.app')
@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
		          	<h3 class="box-title">Atributos Requeridos de: 
		          		<a href="{{ route('ListarCategoriasAdmin',$categoria->id)}}">
		          			{{$categoria->nombre}}
		          		</a>
		          	</h3>
		          	<div class="box-tools pull-right">
		                <a href="{{ route('CrearAtributoCategoriaAdmin',$categoria->id)}}">
			                <button type="button" class="btn btn-box-tool">
			                	<i class="fa fa-plus"></i>
			                </button>
		                </a>
		            </div>
		        </div>
		        <div class="box-body">
					<table id="requeridos" class="table table-bordered table-striped">
						<thead>
							<tr>
							  	<th>#</th>
							  	<th>Nombre</th>
							  	<th>Tipo</th>
							  	<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							@php
								$seleccionados=[];
							@endphp
							@foreach ($requeridos as $id => $requerido)
								<tr>
								  	<td>{{$id+1}}</td>
								  	<td>{{$requerido->nombre}}</td>
								  	<td>{{$requerido->tipo}}</td>
								  	<td>
								  		<a href="{{route('RetirarAtributoCategoriaAdmin',array($categoria->id,$requerido->id))}}">
											<button class="btn btn-danger btn-xs">
												<i class="fa fa-minus" aria-hidden="true"></i>
											</button>
										</a>
								  		<a href="{{route('EditarAtributoCategoriaAdmin',array($categoria->id,$requerido->id))}}">
											<button class="btn btn-danger btn-xs">
												<i class="fa fa-edit" aria-hidden="true"></i>
											</button>
										</a>
								  		@php
								  			$ruta=route('EliminarAtributoCategoriaAdmin',array($categoria->id,$requerido->id)).'/';
								  		@endphp
										<button class="btn btn-danger btn-xs" onclick="eliminarNano('{{$ruta}}')">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
								  	</td>
								</tr>

								@php
									$seleccionados[]=$requerido->id;
								@endphp
							@endforeach
						</tbody>
						<tfoot>
							<tr>
							  	<th>#</th>
							  	<th>Hijos</th>
							  	<th>Acciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
		          	<h3 class="box-title">Atributos Opcionales en: 
		          		<a href="{{ route('ListarCategoriasAdmin',$padre['id'])}}">
		          			{{$padre['nombre']}}
		          		</a>
		          	</h3>
		        </div>
		        <div class="box-body">
					<table id="opcionales" class="table table-bordered table-striped">
						<thead>
							<tr>
							  	<th>#</th>
							  	<th>Nombre</th>
							  	<th>Tipo</th>
							  	<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($opcionales as $id => $opcional)
								@if(!in_array($opcional->id, $seleccionados))
									<tr>
									  	<td>{{$id+1}}</td>
									  	<td>{{$opcional->nombre}}</td>
									  	<td>{{$opcional->tipo}}</td>
									  	<td>
									  		<a href="{{route('RequerirAtributoCategoriaAdmin',array($categoria->id,$opcional->id))}}">
												<button class="btn btn-danger btn-xs">
													<i class="fa fa-plus" aria-hidden="true"></i>
												</button>
											</a>
									  		<a href="{{route('EditarAtributoCategoriaAdmin',array($categoria->id,$opcional->id))}}">
												<button class="btn btn-danger btn-xs">
													<i class="fa fa-edit" aria-hidden="true"></i>
												</button>
											</a>
									  		@php
								  				$ruta=route('EliminarAtributoCategoriaAdmin',array($categoria->id,$opcional->id)).'/';
									  		@endphp
											<button class="btn btn-danger btn-xs" onclick="eliminarNano('{{$ruta}}')">
												<i class="fa fa-trash" aria-hidden="true"></i>
											</button>
									  	</td>
									</tr>
								@endif
							@endforeach
						</tbody>
						<tfoot>
							<tr>
							  	<th>#</th>
							  	<th>Hijos</th>
							  	<th>Acciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('javascript')
<script type="text/javascript">
	
	$(function () {
		$('#requeridos').DataTable({
		  	"paging": true,
		  	"ordering": true,
		  	"info": true,
		  	"autoWidth": true,
		  	"language": {
		        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
		    }
		});
	});
	
	$(function () {
		$('#opcionales').DataTable({
		  	"paging": true,
		  	"ordering": true,
		  	"info": true,
		  	"autoWidth": true,
		  	"language": {
		        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
		    }
		});
	});
</script>
@endsection