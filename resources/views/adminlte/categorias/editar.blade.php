@extends('layouts.adminlte.app')
@section('content')
	<form action="{{route('UpdateCategoriasAdmin',$categoria->id)}}" method="POST">
		@csrf
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header with-border">
			          	<h3 class="box-title">Editar Categoria: {{$categoria->nombre}}</h3>
			        </div>
			        <div class="box-body">
						<div class="form-group">
							<label for="nombre">Nombre</label>
							<input type="text" name="nombre" value="{{$categoria->nombre}}" class="form-control" placeholder="nombre">
							<input type="hidden" name="padre_id" value="{{$padre_id}}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
			        <div class="box-body">
						<div class="form-group">
							<center>
								<br>
								<input type="submit" value="Guardar" class="btn btn-info" placeholder="nombre">
							</center>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
		
@endsection