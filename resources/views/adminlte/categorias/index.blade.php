@extends('layouts.adminlte.app')
@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
					@if($padre->id == 0)
		          		<h3 class="box-title">Lista de Categorias</h3>
					@else
		          		<h3 class="box-title">Lista de subcategorias de: <a href="{{ route('ListarCategoriasAdmin',$padre->padre_id)}}">{{$padre->nombre}}</a></h3>
					@endif
		          	<div class="box-tools pull-right">
		                <a href="{{ route('CrearCategoriasAdmin').'/'.$padre->id}}">
			                <button type="button" class="btn btn-box-tool">
			                	<i class="fa fa-plus"></i>
			                </button>
		                </a>
		            </div>
		        </div>
		        <div class="box-body">
					<table id="categorias" class="table table-bordered table-striped">
						<thead>
							<tr>
							  	<th>#</th>
							  	<th>Nombre</th>
							  	<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($categorias as $id => $categoria)
								<tr>
								  	<td>{{$id+1}}</td>
								  	<td>{{$categoria->nombre}}</td>
								  	<td>
								  		<a href="{{route('ListarCategoriasAdmin').'/'.$categoria->id}}">
											<button class="btn btn-danger btn-xs"">
												<i class="fa fa-eye" aria-hidden="true"></i>
											</button>
								  		</a>
								  		<a href="{{route('ListarAtributoCategoriaAdmin',$categoria->id)}}">
											<button class="btn btn-danger btn-xs">
												<i class="fab fa-creative-commons-share"></i>
											</button>
										</a>
								  		<a href="{{route('EditarCategoriasAdmin',$categoria->id)}}">
											<button class="btn btn-danger btn-xs">
												<i class="fa fa-edit" aria-hidden="true"></i>
											</button>
										</a>
								  		@php
								  			$ruta=route('EliminarCategoriasAdmin',$categoria->id).'/';
								  		@endphp
										<button class="btn btn-danger btn-xs" onclick="eliminarNano('{{$ruta}}')">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
								  	</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
							  	<th>#</th>
							  	<th>Hijos</th>
							  	<th>Acciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('javascript')
<script type="text/javascript">
	
	$(function () {
		$('#categorias').DataTable({
		  	"paging": true,
		  	"ordering": true,
		  	"info": true,
		  	"autoWidth": true,
		  	"language": {
		        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
		    }
		});
	});
</script>
@endsection