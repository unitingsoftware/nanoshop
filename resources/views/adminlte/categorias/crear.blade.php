@extends('layouts.adminlte.app')
@section('content')
	<form action="{{route('GuardarCategoriaAdmin')}}" method="POST">
		@csrf
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header with-border">
			          	<h3 class="box-title">Crear Categoria: {{$padre}}</h3>
			        </div>
			        <div class="box-body">
						<div class="form-group">
							<label for="nombre">Nombre</label>
							<input type="text" required name="nombre" class="form-control" placeholder="nombre">
							<input type="hidden" required name="padre_id" value="{{$padre_id}}">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
			        <div class="box-body">
						<div class="form-group">
							<center>
								<br>
								<input type="submit" value="Guardar" class="btn btn-info" placeholder="nombre">
							</center>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
		
@endsection