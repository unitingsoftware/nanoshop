@extends('layouts.adminlte.app')
@section('content')
	<style type="text/css">
		.galeria_img{
		  	width: 100%;
		  	margin: 0 auto;
		}
		.square{
		  	font-family: sans-serif;
		  	color: white;
		  	padding: 5px;
		    float:left;
		    width: 15%;
		    margin-right:2%;
		    margin-top:5px;
		    background: deepskyblue;
		    background-size: cover;
		}
		.square:last-child{
		  	margin-right:0;
		}
		.square:before{
		  	content:"";
		  	display:block;
		  	padding-top:100%;
		  	float: left; 
		}
	</style>
		<div class="row">
			<div class="col-xs-2">
			</div>
			<div class="col-xs-8">
				<div class="box">
					<div class="box-header with-border">
			          	<h3 class="box-title">Datos del Cliente</h3>
			        </div>
			        <div class="box-body">
						<div class="form-group">
							<label for="nombre">Nombre:</label> {{$presupuesto->name}}
						</div>
						<div class="form-group">
							<label for="nombre">Correo:</label> {{$presupuesto->email}}
						</div>
						<div class="form-group">
							<label for="nombre">Telefono:</label> {{$presupuesto->telefono}}
						</div>
					</div>
				</div>
			
				<div class="box">
					<div class="box-header with-border">
			          	<h3 class="box-title">Datos del Articulo</h3>
			        </div>
			        <div class="box-body">
						<div class="form-group">
							<label for="nombre">Articulo:</label> {{$presupuesto->articulo->nombre}}
						</div>
			        	


					</div>
				</div>
				<div class="box">
					<div class="box-header with-border">
			          	<h3 class="box-title">Estado</h3>
			        </div>
			        <div class="box-body">
			        	<form action="{{route('CambioEstadoPresupuestoAdmin',$presupuesto->id)}}" method="POST">
			        		@csrf
							<div class="form-group">
								<label for="nombre">Estado Actual:</label> {{$presupuesto->estado}}
							</div>
							<div class="form-group">
								<label for="nombre">Nuevo Estado:</label>
								<select name="estado" required=""  class="form-control">
									<option {{($presupuesto->estado == 'Pendiente' ? "selected":"")}}>Pendiente</option>
									<option {{($presupuesto->estado == 'Completado' ? "selected":"")}}>Completado</option>
								</select>
							</div>
							<div class="form-group">
								<input type="submit" name="" class="form-control btn btn-success">
							</div>
			        	</form>
					</div>
				</div>
			</div>
			<div class="col-xs-4">
			</div>
		</div>
@endsection

@section('javascript')
	

@endsection