@extends('layouts.adminlte.app')
@section('content')
	<style type="text/css">
		.galeria_img{
		  	width: 100%;
		  	margin: 0 auto;
		}
		.square{
		  	font-family: sans-serif;
		  	color: white;
		  	padding: 5px;
		    float:left;
		    width: 15%;
		    margin-right:2%;
		    margin-top:5px;
		    background: deepskyblue;
		    background-size: cover;
		}
		.square:last-child{
		  	margin-right:0;
		}
		.square:before{
		  	content:"";
		  	display:block;
		  	padding-top:100%;
		  	float: left; 
		}
	</style>
		<div class="row">
			<div class="col-xs-8">
				<div class="box">
					<div class="box-header with-border">
			          	<h3 class="box-title">Articulos Pedido#{{$pedido->id}}</h3>
			        </div>
			        <div class="box-body">
			        	<table id="pedidos" class="table table-bordered table-striped">
							<thead>
								<tr>
								  	<th>#</th>
								  	<th>Nombre</th>
								  	<th>Precio</th>
								  	<th>Cantidad</th>
								  	<th>Totales</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($pedido->articulos_pedido as $id => $articulo)
									<tr>
									  	<td>{{$id+1}}</td>
									  	<td><a href="{{route('EditarArticulosAdmin',$articulo->id)}}">{{$articulo->nombre}}</a></td>
									  	<td>{{$articulo->pivot->precio}}</td>
									  	<td>{{$articulo->pivot->cantidad}}</td>
									  	<td>${{$articulo->pivot->cantidad*$articulo->pivot->precio}}</td>
									</tr>
								@endforeach
								@for ($i=$id; $i < 9; $i++) 
									<tr>
									  	<td><br></td>
									  	<td></td>
									  	<td></td>
									  	<td></td>
									  	<td></td>
									</tr>
								@endfor
							</tbody>
							<tfoot>
								<tr>
								  	<th colspan="4" style="text-align: right;">Total</th>
								  	<th>${!!$pedido->total!!}</th>
								</tr>
							</tfoot>
						</table>
					</div>
					
				</div>
			</div>
			<div class="col-xs-4">
				<div class="box">
					<div class="box-header with-border">
			          	<h3 class="box-title">Datos del Cliente</h3>
			        </div>
			        <div class="box-body">
						<div class="form-group">
							<label for="nombre">Usuario:</label> 
							<a href="{{route('VerUsuariosAdmin',$pedido->user->id)}}">
								{{$pedido->user->user}}
							</a>
						</div>
						<div class="form-group">
							<label for="nombre">Nombre:</label> {{$pedido->user->name}}
						</div>
						<div class="form-group">
							<label for="nombre">Correo:</label> {{$pedido->user->email}}
						</div>
						<div class="form-group">
							<label for="nombre">Telefono:</label> {{$pedido->user->telefono}}
						</div>
					</div>
				</div>

				<div class="box">
					<div class="box-header with-border">
			          	<h3 class="box-title">Estado</h3>
			        </div>
			        <div class="box-body">
			        	<form action="{{route('CambioEstadoPedidoAdmin',$pedido->id)}}" method="POST">
			        		@csrf
							<div class="form-group">
								<label for="nombre">Estado Actual:</label> {{$pedido->estado}}
							</div>
							<div class="form-group">
								<label for="nombre">Nuevo Estado:</label>
								<select name="estado" required=""  class="form-control">
									<option {{($pedido->estado == 'Pendiente' ? "selected":"")}}>Pendiente</option>
									<option {{($pedido->estado == 'En Proceso' ? "selected":"")}}>En Proceso</option>
									<option {{($pedido->estado == 'Enviado' ? "selected":"")}}>Enviado</option>
									<option {{($pedido->estado == 'Entregado' ? "selected":"")}}>Entregado</option>
									<option {{($pedido->estado == 'Rechazado' ? "selected":"")}}>Rechazado</option>
								</select>
							</div>
							<div class="form-group">
								<input type="submit" name="" class="form-control btn btn-success">
							</div>
			        	</form>
					</div>
				</div>
			</div>
		</div>
@endsection

@section('javascript')
	

@endsection