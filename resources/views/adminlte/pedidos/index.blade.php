@extends('layouts.adminlte.app')
@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
		          	<h3 class="box-title">Lista de Pedidos</h3>
		          	<div class="box-tools pull-right">

		            </div>
		        </div>
		        <div class="box-body">
					<table id="pedidos" class="table table-bordered table-striped">
						<thead>
							<tr>
							  	<th>#</th>
							  	<th>Usuario</th>
							  	<th>Articulos</th>
							  	<th>Total</th>
							  	<th>Estado</th>
							  	<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($pedidos as $id => $pedido)
								<tr>
								  	<td>{{$id+1}}</td>
								  	<td>{{$pedido->user->name}}</td>
								  	<td>{{$pedido->articulos}}</td>
								  	<td>${{$pedido->total}}</td>
								  	<td>{{$pedido->estado}}</td>
								  	<td>
								  		<a href="{{route('VerPedidoAdmin',$pedido->id)}}">
											<button class="btn btn-danger btn-xs">
												<i class="fa fa-eye" aria-hidden="true"></i>
											</button>
								  		</a>
								  	</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
							  	<th>#</th>
							  	<th>Usuario</th>
							  	<th>Articulos</th>
							  	<th>Total</th>
							  	<th>Estado</th>
							  	<th>Acciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('javascript')
<script type="text/javascript">
	
	$(function () {
		$('#pedidos').DataTable({
		  	"paging": true,
		  	"ordering": true,
		  	"info": true,
		  	"autoWidth": true,
		  	"language": {
		        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
		    }
		});
	});
</script>
@endsection