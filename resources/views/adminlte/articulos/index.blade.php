@extends('layouts.adminlte.app')
@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
	          		<h3 class="box-title">Lista de Articulos</h3>
	          		<div class="box-tools pull-right">
		                <a href="{{ route('CrearArticuloAdmin')}}">
			                <button type="button" class="btn btn-box-tool">
			                	<i class="fa fa-plus"></i>
			                </button>
		                </a>
		            </div>
		        </div>
		        <div class="box-body">
					<table id="articulos" class="table table-bordered table-striped">
						<thead>
							<tr>
							  	<th>#</th>
							  	<th>Nombre</th>
							  	<th>Precio</th>
							  	<th>Categoria</th>
							  	<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($articulos as $id => $articulo)
								<tr>
								  	<td>{{$id+1}}</td>
								  	<td>{{$articulo->nombre}}</td>
								  	<td>{{$articulo->precio}}</td>
								  	<td>{{$articulo->categoria->nombre}}</td>
								  	<td>
								  		<a href="{{route('EditarArticulosAdmin',$articulo->id)}}">
											<button class="btn btn-danger btn-xs">
												<i class="fa fa-edit" aria-hidden="true"></i>
											</button>
										</a>

								  		@php
								  			$ruta=route('EliminarArticulosAdmin',$articulo->id).'/';
								  		@endphp
										<button class="btn btn-danger btn-xs" onclick="eliminarNano('{{$ruta}}')">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
								  	</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
							  	<th>#</th>
							  	<th>Nombre</th>
							  	<th>Precio</th>
							  	<th>Categoria</th>
							  	<th>Acciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('javascript')
<script type="text/javascript">
	
	$(function () {
		$('#articulos').DataTable({
		  	"paging": true,
		  	"ordering": true,
		  	"info": true,
		  	"autoWidth": true,
		  	"language": {
		        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
		    }
		});
	});
</script>
@endsection