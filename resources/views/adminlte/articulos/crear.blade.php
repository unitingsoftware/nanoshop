@extends('layouts.adminlte.app')
@section('content')
	<style type="text/css">
		.galeria_img{
		  	width: 100%;
		  	margin: 0 auto;
		}
		.square{
		  	font-family: sans-serif;
		  	color: white;
		  	padding: 5px;
		    float:left;
		    width: 15%;
		    margin-right:2%;
		    margin-top:5px;
		    background: deepskyblue;
		    background-size: cover;
		}
		.square:last-child{
		  	margin-right:0;
		}
		.square:before{
		  	content:"";
		  	display:block;
		  	padding-top:100%;
		  	float: left; 
		}
	</style>
	<form action="{{route('GuardarArticuloAdmin')}}" method="POST">
		@csrf
		<div class="row">
			<div class="col-xs-8">
				<div class="box">
					<div class="box-header with-border">
			          	<h3 class="box-title">Información</h3>
			        </div>
			        <div class="box-body">
						<div class="form-group">
							<label for="nombre">Nombre</label>
							<input type="text" required name="nombre" class="form-control" placeholder="Nombre del Articulo">
						</div>
						<div class="form-group">
							<label for="nombre">Precio</label>
							<input type="number" step="0.01" required name="precio" class="form-control" placeholder="Precio del Articulo">
							<input type="hidden" name="presupuesto" value="1" required="required" >
						</div>
						<div class="form-group">
							<label for="nombre">Categoria</label>
							<select name="categoria_id" required="required" class="form-control" onchange="buscarAtributoCategoria(value)" >
								<option value="">Seleccionar</option>
								@foreach($categoria as $categoria)
									<option value="{{$categoria['id']}}">{{$categoria['nombre']}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="nombre">Descripción</label>
							<textarea class="form-control" rows="8" name="descripcion" placeholder="Click..."></textarea>
						</div>
					</div>
					<div id="lista_atributos">
						<div class="box-header with-border">
				          	<h3 class="box-title">Atributos</h3>
				        </div>
				        <div class="box-body">
					        <div class="form-group">
								<label for="nombre">Sin atributos disponibles</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="box">
					<div class="box-header with-border">
			          	<h3 class="box-title">Imagenes</h3>
			        </div>
			        <div class="box-body">
						<div class="form-group">
							<label for="nombre">Foto Principal</label>
							<input type="hidden" class="form-control" id="foto" name="foto">
							<div id="foto_img" onclick="openKCFinderFoto('foto','foto_img')" style="min-height:100px;width:100%; cursor:pointer;">
								<img src="{{asset('imagenes/muestras/seleccionar.jpg')}}"  style="width:100%;" >
							</div>
						</div>

						<div class="form-group">
							<label for="nombre">Galeria de Imagenes</label>
							<input type="hidden" class="form-control" id="galeria" name="galeria">
							@php
								$original=asset('imagenes/muestras/seleccionar_galeria.jpg');
							@endphp
							<div id="galeria_img" onclick="openKCFinderGaleria('galeria','galeria_img','{{$original}}')" style="min-height:100px;width:100%; cursor:pointer;">
								<img src="{{$original}}"  style="width:100%;" >
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
			        <div class="box-body">
						<div class="form-group">
							<center>
								<br>
								<input type="submit" value="Guardar" class="btn btn-info" placeholder="nombre">
							</center>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@endsection

@section('javascript')
	<script type="text/javascript">
		function openKCFinderFoto(field,campo) {
			window.KCFinder = {
			    callBack: function(url) {
			        document.getElementById(campo).innerHTML = '<img src="'+url+'" style="width:100%;" >';
			        document.getElementById(field).value = url;
			        window.KCFinder = null;
			    }
			};
			window.open('{{asset('kcfinder/browse.php?lang=es&type=files&dir=files/public')}}', 'kcfinder_textbox',
			    'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
			    'resizable=1, scrollbars=0, width=800, height=600'
			);
		}

		function openKCFinderGaleria(field,campo,original) {
		    window.KCFinder = {
		        callBackMultiple: function(files) {
		            window.KCFinder = null;
		            document.getElementById(field).value = '';
		            document.getElementById(campo).innerHTML = '<img src="'+original+'" style="width:100%;" >';
		            for (var i = 0; i < files.length; i++){
		            	if (i == 0) {
		            		document.getElementById(campo).innerHTML = '<div class="square" style="background-image:url('+files[i]+');"></div>';
		            	}else{
			            	if ((i+1) % 6 === 0) {
		            			document.getElementById(campo).innerHTML += '<div class="square" style="background-image:url('+files[i]+'); margin-right:0;"></div>';
							}else{
		            			document.getElementById(campo).innerHTML += '<div class="square" style="background-image:url('+files[i]+');"></div>';
							}
		            	}
		                document.getElementById(field).value += files[i]+';';
		            }
		        }
		    };
		    window.open('{{asset('kcfinder/browse.php?lang=es&type=files&dir=files/public')}}',
		        'kcfinder_multiple', 'status=0, toolbar=0, location=0, menubar=0, ' +
		        'directories=0, resizable=1, scrollbars=0, width=800, height=600'
		    );
		}

	    function buscarAtributoCategoria(categoria){
	    	$.get("{{route('ArticuloAtributoCategoriaAdmin')}}/"+categoria, function(atributos){
	            $("#lista_atributos").html(atributos);
	        });
	    }
	</script>

@endsection