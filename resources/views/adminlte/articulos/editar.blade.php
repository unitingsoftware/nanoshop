@extends('layouts.adminlte.app')
@section('content')
	<style type="text/css">
		.galeria_img{
		  	width: 100%;
		  	margin: 0 auto;
		}
		.square{
		  	font-family: sans-serif;
		  	color: white;
		  	padding: 5px;
		    float:left;
		    width: 15%;
		    margin-right:2%;
		    margin-top:5px;
		    background: deepskyblue;
		    background-size: cover;
		}
		.square:last-child{
		  	margin-right:0;
		}
		.square:before{
		  	content:"";
		  	display:block;
		  	padding-top:100%;
		  	float: left; 
		}
	</style>
	<form action="{{route('UpdateArticulosAdmin',$articulo->id)}}" method="POST">
		@csrf
		<div class="row">
			<div class="col-xs-8">
				<div class="box">
					<div class="box-header with-border">
			          	<h3 class="box-title">Información</h3>
			        </div>
			        <div class="box-body">
						<div class="form-group">
							<label for="nombre">Nombre</label>
							<input type="text" required name="nombre" value="{{$articulo->nombre}}" class="form-control" placeholder="Nombre del Articulo">
						</div>
						<div class="form-group">
							<label for="nombre">Precio</label>
							<input type="number" step="0.01" required name="precio" value="{{$articulo->precio}}" class="form-control" placeholder="Precio del Articulo">
							<input type="hidden" name="presupuesto" value="1" required="required" >						
						</div>
						<div class="form-group">
							<label for="nombre">Modo Presupuesto</label>
							<select name="presupuesto" required="required" class="form-control" onchange="buscarAtributoCategoria(value)" >
								<option {{($articulo->presupuesto == 0 ? "selected":"")}} value="0">No</option>
								<option {{($articulo->presupuesto == 1 ? "selected":"")}} value="1">Si</option>
							</select>
						</div>
						<div class="form-group">
							<label for="nombre">Categoria</label>
							<select name="categoria_id" required="required" class="form-control" onchange="buscarAtributoCategoria(value)" >
								<option value="">Seleccionar</option>
								@foreach($categoria as $categoria)
									@if ($articulo->categoria->id != $categoria['id'])
										<option value="{{$categoria['id']}}">{{$categoria['nombre']}}</option>
									@else
										<option selected value="{{$categoria['id']}}">{{$categoria['nombre']}}</option>
									@endif
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="nombre">Descripción</label>
							<textarea class="form-control" rows="8" name="descripcion" placeholder="Click...">{{$articulo->descripcion}}</textarea>
						</div>
					</div>
					<div id="lista_atributos">
						@if($requeridos or $opcionales)
					        @php
			            		$i=0;
							@endphp		
							@if($requeridos)
								<div class="box-header with-border">
					                <h3 class="box-title">Atributos Requeridos</h3>
					            </div>
					            <div class="box-body">
					            	@php
					            		foreach ($requeridos as $key => $requerido) {
					            			if(isset($requerido->pivot)){
					            				$pivote=$requerido->pivot->contenido;
					            			}else{
					            				$pivote=false;
					            			}

								            switch ($requerido->tipo) {
								                case 'select':
								                    @endphp
								                        <div class="form-group">
								                            <label for="nombre">{{$requerido->nombre}}</label>
								                            <select required class="form-control" name="atributo[{{$i}}][value]">
								                                <option value="">Seleccionar</option>
								                                @php
								                                    $lista=explode(",", $requerido->contenido);
							                                        foreach ($lista as  $elemento) {
							                                        	@endphp
							                                            	<option {{($pivote == $elemento ? "selected":"")}} >{{$elemento}}</option>
							                                        	@php
							                                        }
								                                @endphp
								                            </select>
								                        </div>
								                    @php                    
								                break;
								                case 'number_list':
								                	if($pivote){
								                		$valores=explode("|_|",$pivote);
								                		$valor1=$valores[0];
								                		$valor2=$valores[1];
								                	}else{
								                		$valor1=false;
								                		$valor2=false;
								                	}
								                    @endphp
								                        <div class="form-group">
								                            <label for="nombre" style="width: 100%;">{{$requerido->nombre}}</label>
								                            <div class="col-md-6" style="padding-left: 0px;">
								                                <input type="number" value="{{$valor1}}" class="form-control" name="atributo[{{$i}}][value]" required="required">
								                            </div>
								                            <div class="col-md-6" style="padding-right: 0px;">
								                                <select class="form-control" name="atributo[{{$i}}][lista]" required="required">
								                                    <option value="">Seleccionar</option>
								                                    @php
								                                        $lista=explode(",", $requerido->contenido);
								                                        foreach ($lista as  $elemento) {
								                                        	@endphp
								                                            	<option {{($valor2 == $elemento ? "selected":"")}}>{{$elemento}}</option>
								                                        	@php
								                                        }
								                                    @endphp
								                                </select>
								                                <br>
								                            </div>
								                        </div>
								                    @php
								                break;
								                case 'text':
								                    @endphp
								                        <div class="form-group">
								                            <label for="nombre">{{$requerido->nombre}}</label>
								                            <input required value="{{$pivote}}" type="text" class="form-control" name="atributo[{{$i}}][value]">
								                        </div>
								                    @php                    
								                break;
								                case 'number':
								                    @endphp
								                        <div class="form-group">
								                            <label for="nombre">{{$requerido->nombre}}</label>
								                            <input required value="{{$pivote}}" type="number" class="form-control" name="atributo[{{$i}}][value]">
								                        </div>
								                    @php                    
								                break;
								                
								                default:
								                    # code...
								                break;
								            }
							            	@endphp
						            			<input type="hidden" class="form-control" name="atributo[{{$i}}][id]" value="{{$requerido->id}}">
							            	@php
							            	$i++;
								        }
						            @endphp
					            </div>
				            @endif

				            @if($opcionales)
								<div class="box-header with-border">
					                <h3 class="box-title">Atributos Opcionales</h3>
					            </div>
					            <div class="box-body">
					            	@php
					            		foreach ($opcionales as $key => $opcional) {
								            if(isset($opcional->pivot)){
					            				$pivote=$opcional->pivot->contenido;
					            			}else{
					            				$pivote=false;
					            			}
								            switch ($opcional->tipo) {
								                case 'select':
								                    @endphp
								                        <div class="form-group">
								                            <label for="nombre">{{$opcional->nombre}}</label>
								                            <select  class="form-control" name="atributo[{{$i}}][value]">
								                                <option value="">Seleccionar</option>
								                                @php
								                                    $lista=explode(",", $opcional->contenido);
								                                    foreach ($lista as  $elemento) {
								                                        @endphp
								                                        	<option {{($pivote == $elemento ? "selected":"")}} >{{$elemento}}</option>
								                                        @php
								                                    }
								                                @endphp
								                            </select>
								                        </div>
								                    @php                    
								                break;
								                case 'number_list':
								                	if($pivote){
								                		$valores=explode("|_|",$pivote);
							                			if(!isset($valores[0])){$valor1=' ';}else{$valor1=$valores[0];}
							                			if(!isset($valores[1])){$valor2=' ';}else{$valor2=$valores[1];}
								                	}else{
								                		$valor1=false;
								                		$valor2=false;
								                	}
								                    @endphp
								                        <div class="form-group">
								                            <label for="nombre" style="width: 100%;">{{$opcional->nombre}}</label>
								                            <div class="col-md-6" style="padding-left: 0px;">
								                                <input type="number" value="{{$valor1}}" class="form-control" name="atributo[{{$i}}][value]">
								                            </div>
								                            <div class="col-md-6" style="padding-right: 0px;">
								                                <select  class="form-control" name="atributo[{{$i}}][lista]">
								                                    <option value="">Seleccionar</option>
								                                    @php
								                                        $lista=explode(",", $opcional->contenido);
								                                        foreach ($lista as  $elemento) {
								                                        	@endphp
								                                            	<option {{($valor2 == $elemento ? "selected":"")}}>{{$elemento}}</option>
								                                        	@php
								                                        }
								                                    @endphp
								                                </select>
								                                <br>
								                            </div>
								                        </div>
								                    @php
								                break;
								                case 'text':
								                    @endphp
								                        <div class="form-group">
								                            <label for="nombre">{{$opcional->nombre}}</label>
								                            <input  value="{{$pivote}}" type="text" class="form-control" name="atributo[{{$i}}][value]">
								                        </div>
								                    @php                    
								                break;
								                case 'number':
								                    @endphp
								                        <div class="form-group">
								                            <label for="nombre">{{$opcional->nombre}}</label>
								                            <input  value="{{$pivote}}" type="number" class="form-control" name="atributo[{{$i}}][value]">
								                        </div>
								                    @php                    
								                break;
								                
								                default:
								                    # code...
								                break;
								            }
							            	@endphp
							            		<input type="hidden" class="form-control" name="atributo[{{$i}}][id]" value="{{$opcional->id}}">
							            	@php
							            	$i++;
								        }
					            	@endphp
					            </div>
				            @endif
						@else
							<div class="box-header with-border">
					          	<h3 class="box-title">Atributos</h3>
					        </div>
					        <div class="box-body">
						        <div class="form-group">
									<label for="nombre">Sin atributos disponibles</label>
								</div>
							</div>
						@endif
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="box">
					<div class="box-header with-border">
			          	<h3 class="box-title">Imagenes</h3>
			        </div>
			        <div class="box-body">
						<div class="form-group">
							@php
								$foto_muestra=asset('imagenes/muestras/seleccionar.jpg');
								$original=asset('imagenes/muestras/seleccionar_galeria.jpg');
							@endphp
							<label for="nombre">Foto Principal</label>
							<input type="hidden" class="form-control" id="foto" name="foto" value="{{$articulo->foto}}">
							<div id="foto_img" onclick="openKCFinderFoto('foto','foto_img')" style="min-height:100px;width:100%; cursor:pointer;">
								@if ($articulo->foto)
									<img src="{{$articulo->foto}}"  style="width:100%;" >
								@else
									<img src="{{$foto_muestra}}"  style="width:100%;" >
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="nombre">Galeria de Imagenes</label>
							<input type="hidden" class="form-control" id="galeria" name="galeria" value="{{$articulo->galeria}}">
							<div id="galeria_img" onclick="openKCFinderGaleria('galeria','galeria_img','{{$original}}')" style="min-height:100px;width:100%; cursor:pointer;">
								@php 
									if ($articulo->galeria != null) {
										$galeria=explode(';', $articulo->galeria);
										foreach ($galeria as $i => $value) {
											# code...
							            	if ($i == 0) {
							            		echo'<div class="square" style="background-image:url('.$value.');"></div>';
							            	}else{
								            	if (($i+1) % 6 === 0) {
							            			echo'<div class="square" style="background-image:url('.$value.'); margin-right:0;"></div>';
												}else{
							            			echo'<div class="square" style="background-image:url('.$value.');"></div>';
												}
							            	}
										}
									}else{
										echo '<img src="'.$original.'"  style="width:100%;" >';
									}

								@endphp
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
			        <div class="box-body">
						<div class="form-group">
							<center>
								<br>
								<input type="submit" value="Guardar" class="btn btn-info" placeholder="nombre">
							</center>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@endsection

@section('javascript')
	<script type="text/javascript">
		function openKCFinderFoto(field,campo) {
			window.KCFinder = {
			    callBack: function(url) {
			        document.getElementById(campo).innerHTML = '<img src="'+url+'" style="width:100%;" >';
			        document.getElementById(field).value = url;
			        window.KCFinder = null;
			    }
			};
			window.open('{{asset('kcfinder/browse.php?lang=es&type=files&dir=files/public')}}', 'kcfinder_textbox',
			    'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
			    'resizable=1, scrollbars=0, width=800, height=600'
			);
		}

		function openKCFinderGaleria(field,campo,original) {
		    window.KCFinder = {
		        callBackMultiple: function(files) {
		            window.KCFinder = null;
		            document.getElementById(field).value = '';
		            document.getElementById(campo).innerHTML = '<img src="'+original+'" style="width:100%;" >';
		            for (var i = 0; i < files.length; i++){
		            	if (i == 0) {
		            		document.getElementById(campo).innerHTML = '<div class="square" style="background-image:url('+files[i]+');"></div>';
		            	}else{
			            	if ((i+1) % 6 === 0) {
		            			document.getElementById(campo).innerHTML += '<div class="square" style="background-image:url('+files[i]+'); margin-right:0;"></div>';
							}else{
		            			document.getElementById(campo).innerHTML += '<div class="square" style="background-image:url('+files[i]+');"></div>';
							}
		            	}
		                document.getElementById(field).value += files[i]+';';
		            }
		        }
		    };
		    window.open('{{asset('kcfinder/browse.php?lang=es&type=files&dir=files/public')}}',
		        'kcfinder_multiple', 'status=0, toolbar=0, location=0, menubar=0, ' +
		        'directories=0, resizable=1, scrollbars=0, width=800, height=600'
		    );
		}

	    function buscarAtributoCategoria(categoria){
	    	$.get("{{route('ArticuloAtributoCategoriaAdmin')}}/"+categoria, function(atributos){
	            $("#lista_atributos").html(atributos);
	        });
	    }
	</script>

@endsection