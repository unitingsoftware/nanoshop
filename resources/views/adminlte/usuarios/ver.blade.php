@extends('layouts.adminlte.app')
@section('content')
	<style type="text/css">
		.galeria_img{
		  	width: 100%;
		  	margin: 0 auto;
		}
		.square{
		  	font-family: sans-serif;
		  	color: white;
		  	padding: 5px;
		    float:left;
		    width: 15%;
		    margin-right:2%;
		    margin-top:5px;
		    background: deepskyblue;
		    background-size: cover;
		}
		.square:last-child{
		  	margin-right:0;
		}
		.square:before{
		  	content:"";
		  	display:block;
		  	padding-top:100%;
		  	float: left; 
		}
	</style>
		<div class="row">
			<div class="col-md-3">

			</div>
			<div class="col-md-6">
				<div class="box">
					<div class="box-header with-border">
			          	<h3 class="box-title">Datos del Usuario</h3>
			        </div>
			        <form class="form-horizontal" method="POST" action="{{route('UpdateUsuariosAdmin',$usuario->id)}}">
			        	@csrf
				        <div class="box-body">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Usuario:</label>

								<div class="col-sm-8">
									<input type="text" class="form-control" name="user" value="{{$usuario->user}}" placeholder="Usuario" required>
								</div>
			                </div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Nombre:</label>

								<div class="col-sm-8">
									<input type="text" class="form-control" name="name" value="{{$usuario->name}}" placeholder="Usuario" required>
								</div>
			                </div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Correo:</label>

								<div class="col-sm-8">
									<input type="email" class="form-control" name="email" value="{{$usuario->email}}" placeholder="Usuario" required>
								</div>
			                </div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Telefono:</label>

								<div class="col-sm-8">
									<input type="tel" class="form-control" name="telefono" value="{{$usuario->telefono}}" placeholder="Usuario" required>
								</div>
			                </div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Cuenta:</label>

								<div class="col-sm-8">
									<select class="form-control" name="type" required>
										<option  {{($usuario->type  == 'admin' ? "selected":"")}} value="admin">Administrador</option>
										<option  {{($usuario->type == 'member' ? "selected":"")}} value="member">Miembro</option>
									</select>
								</div>
			                </div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Nueva Contraseña:</label>

								<div class="col-sm-8">
									<input type="tel" class="form-control" name="password" value="" placeholder="Nueva Contraseña">
								</div>
			                </div>

							<div class="form-group">
								<div class="col-sm-12">
									<input type="submit" class="form-control btn btn-success" style="padding: 10px 0px 30px;" value="Editar Usuario">
								</div>
							</div>
						</div>
			        </form>
				</div>
			</div>
		</div>
@endsection

@section('javascript')
	

@endsection