@extends('layouts.adminlte.app')
@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header with-border">
	          	<h3 class="box-title">Datos del Usuario</h3>
	        </div>
	        <form class="form-horizontal" method="POST" action="{{route('AddUsuariosAdmin')}}">
	        	@csrf
		        <div class="box-body">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Usuario:</label>

						<div class="col-sm-8">
							<input type="text" class="form-control" name="user" value="" placeholder="Usuario" required>
						</div>
	                </div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Nombre:</label>

						<div class="col-sm-8">
							<input type="text" class="form-control" name="name" value="" placeholder="Usuario" required>
						</div>
	                </div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Correo:</label>

						<div class="col-sm-8">
							<input type="email" class="form-control" name="email" value="" placeholder="Usuario" required>
						</div>
	                </div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Telefono:</label>

						<div class="col-sm-8">
							<input type="tel" class="form-control" name="telefono" value="" placeholder="Usuario" required>
						</div>
	                </div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Cuenta:</label>

						<div class="col-sm-8">
							<select class="form-control" name="type" required>
								<option value="admin">Administrador</option>
								<option value="member">Miembro</option>
							</select>
						</div>
	                </div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Contraseña:</label>

						<div class="col-sm-8">
							<input type="password" class="form-control" name="password" value="" placeholder="Usuario" required>
						</div>
	                </div>

					<div class="form-group">
						<div class="col-sm-12">
							<center>
								<input type="submit" class="btn btn-success" style="padding: 10px 20px 10px;" value="Guardar">
							</center>
						</div>
					</div>
				</div>
	        </form>
		</div>
	</div>
</div>

		
@endsection