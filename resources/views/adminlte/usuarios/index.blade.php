@extends('layouts.adminlte.app')
@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
		          	<h3 class="box-title">Lista de Usuarios</h3>
		          	<div class="box-tools pull-right">
		                <a href="{{ route('CrearUsuariosAdmin')}}">
			                <button type="button" class="btn btn-box-tool">
			                	<i class="fa fa-plus"></i>
			                </button>
		                </a>
		            </div>
		        </div>
		        <div class="box-body">
					<table id="usuarios" class="table table-bordered table-striped">
						<thead>
							<tr>
							  	<th>#</th>
							  	<th>Nombre</th>
							  	<th>Correo</th>
							  	<th>Telefono</th>
							  	<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($usuarios as $id => $user)
								<tr>
								  	<td>{{$id+1}}</td>
								  	<td>{{$user->name}}</td>
								  	<td>{{$user->email}}</td>
								  	<td>{{$user->telefono}}</td>
								  	<td>
								  		<a href="{{route('VerUsuariosAdmin',$user->id)}}">
											<button class="btn btn-danger btn-xs"">
												<i class="fa fa-eye" aria-hidden="true"></i>
											</button>
								  		</a>
								  		@php
								  			$ruta=route('EliminarUsuariosAdmin',$user->id);
								  		@endphp
										<button class="btn btn-danger btn-xs" onclick="eliminarNano('{{$ruta}}')">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</button>
								  	</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
							  	<th>#</th>
							  	<th>Hijos</th>
							  	<th>Acciones</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('javascript')
<script type="text/javascript">
	
	$(function () {
		$('#usuarios').DataTable({
		  	"paging": true,
		  	"ordering": true,
		  	"info": true,
		  	"autoWidth": true,
		  	"language": {
		        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
		    }
		});
	});
</script>
@endsection