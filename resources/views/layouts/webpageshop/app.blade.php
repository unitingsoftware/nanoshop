
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
	<!-- Basic Page Needs -->
	<meta charset="UTF-8">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<title>Techno Store - Home Page</title>

	<meta name="author" content="CreativeLayers">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Boostrap style -->
	<link rel="stylesheet" type="text/css" href="{{ asset('webpageshop/stylesheets/bootstrap.min.css') }}">

	<!-- Theme style -->
	<link rel="stylesheet" type="text/css" href="{{ asset('webpageshop/stylesheets/style.css') }}">

	<!-- Reponsive -->
	<link rel="stylesheet" type="text/css" href="{{ asset('webpageshop/stylesheets/responsive.css') }}">

	<!-- Colors -->
	<link rel="stylesheet" type="text/css" href="{{ asset('webpageshop/stylesheets/colors/color2.css') }}" id="colors">

	<link rel="shortcut icon" href="{{ asset('webpageshop/favicon/favicon.png') }}">
	<style type="text/css">
		.top-search form.form-search .cat-wrap p {
		    height: 20px;
		    line-height: 20px;
		    padding: 0px 40px 0 10px;
		    border-radius: 0px;
		    border: 0px !important;
		    display: block;
		    cursor: pointer;
		}
		.top-search form.form-search .cat-wrap .all-categories {
		    width: 235px!important;
		}

		.top-search form.form-search .cat-wrap {
		    width: 200px;
		    cursor: pointer;
		}

		.top-search form.form-search .cat-wrap p{
		    text-align: right;
		}
		.box-cart .inner-box ul.menu-compare-wishlist li a {
		    position: relative;
		}
		.box-cart .inner-box ul.menu-compare-wishlist li span {
		    top: -3px;
		    right: -8px;
		    background-color: #0153a6;
		    width: 20px;
		    height: 20px;
		    line-height: 18px;
		    color: #f4f4f4;
		    font-size: 11px;
		    border-radius: 50%;
		    position: absolute;
		}
		#mainnav > ul.menu {
		    word-spacing: 0px;
		}
		.col-sm-6, .col-sm-12 {
		    float: left;
		}
	</style>
	<script type="text/javascript">
		function SeleccionarCategoriaBuscador(id,nombre){
			nombre= nombre.replace('&nbsp;&nbsp;&nbsp;&nbsp;','');
			nombre= nombre.replace('&nbsp;&nbsp;&nbsp;&nbsp;','');
			nombre= nombre.replace('&nbsp;&nbsp;&nbsp;&nbsp;','');
			nombre= nombre.replace('&nbsp;&nbsp;&nbsp;&nbsp;','');
			nombre= nombre.replace('&nbsp;&nbsp;&nbsp;&nbsp;','');

			document.getElementById('categoria_buscador').innerHTML = nombre;
			document.getElementById('categoria_input').value = id;
		}
	</script>
</head>
<body class="header_sticky">
	<div class="boxed">
			<div class="overlay"></div>

		<!--
			<div class="preloader">
				<div class="clear-loading loading-effect-2">
					<span></span>
				</div>
			</div>
		-->

		<section id="header" class="header">
			<div class="header-top">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<!-- <ul class="flat-support">
								<li>
									<a href="faq.html" title="">Support</a>
								</li>
								<li>
									<a href="store-location.html" title="">Store Locator</a>
								</li>
								<li>
									<a href="order-tracking.html" title="">Track Your Order</a>
								</li>
							</ul> -->
						</div>
						<div class="col-md-4">
							<!-- <ul class="flat-infomation">
								<li class="phone">
									Call Us: <a href="#" title="">+54 11 4788-3174</a>
								</li>
<<<<<<< HEAD
							</ul><!-- /.flat-infomation -->
						</div><!-- /.col-md-4 -->
						<div class="col-md-4">
						<!--
							<ul class="flat-unstyled">
=======
								<li class="email">
									Call Us: <a href="#" title="">info@nanotecsrl.com.ar</a>
								</li>
							</ul> -->
						</div>
						<div class="col-md-4 text-right">
							<h6 style="margin-top: 10px; margin-bottom: 10px;"><i class="fa fa-envelope-o"></i> info@nanotecsrl.com.ar    <i class="fa fa-phone" style="margin-left: 10px;"></i>+54 11 4788-3174</h6>
							<!-- <ul class="flat-unstyled">
>>>>>>> master
								<li class="account">
									@if(auth()->user())
										<a href="#" title="">{{auth()->user()->name}}<i class="fa fa-angle-down" aria-hidden="true"></i></a>
									@else
										<a href="#" title="">Mi Cuenta<i class="fa fa-angle-down" aria-hidden="true"></i></a>
									@endif
									<ul class="unstyled">
										@if(!auth()->user())
										<li>
											<a href="{{route('TiendaUsuariosLoginFront')}}" title="Mi Cuenta">Login</a>
										</li>
										@endif
										<li>
											<a href="{{route('TiendaCarritoVerFront')}}" title="">Carrito</a>
										</li>
										<li>
											<a href="{{route('TiendaMiCuentaFront')}}" title="">Mi Cuenta</a>
										</li>
										<li>
											<a href="{{route('TiendaPedidoListarFront')}}" title="">Pedidos</a>
										</li>
										@if(auth()->user())
											<li>
												<a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
							                      Salir &nbsp;&nbsp;<i class="fa fa-sign-out"></i>
							                    </a>
							                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							                        {{ csrf_field() }}
							                    </form>
											</li>
										@endif
									</ul>
								</li>
<<<<<<< HEAD
							</ul>
						-->
=======
							</ul> -->
>>>>>>> master
						</div><!-- /.col-md-4 -->
					</div><!-- /.row -->
				</div><!-- /.container -->
			</div><!-- /.header-top -->
			<div class="header-middle">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<div id="logo" class="logo">
								<a href="index.html" title="">
									<img src="{{asset('images/logos/logo.png')}}" alt="">
								</a>
							</div><!-- /#logo -->
						</div><!-- /.col-md-3 -->
						<div class="col-md-8">
							<div class="top-search">
								<form action="{{route('TiendaBuscadorArticuloFront')}}" method="get" class="form-search" accept-charset="utf-8">
									<div class="cat-wrap">
										@if(isset($categoria))
											@if($categoria)
												<p id="categoria_buscador">{{$categoria->nombre}}</p>
												<input type="hidden" name="categoria" value="{{$categoria->id}}" id="categoria_input">
											@else
												<p id="categoria_buscador">Toda la Tienda</p>
												<input type="hidden" name="categoria" value="0" id="categoria_input">
											@endif
										@else
											<p id="categoria_buscador">Toda la Tienda</p>
											<input type="hidden" name="categoria" value="0" id="categoria_input">
										@endif
										<span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
										<div class="all-categories">
											<div class="cat-list-search" style="width: 230px;">
												<ul>
													<li onclick="SeleccionarCategoriaBuscador(0,'Todas')">Todas</li>
													@foreach($categorias_buscador AS $categoria)
														<li onclick="SeleccionarCategoriaBuscador({{$categoria['id']}},'{{$categoria['nombre']}}')">{!!$categoria['nombre']!!}</li>
													@endforeach
												</ul>
											</div><!-- /.cat-list-search -->
										</div><!-- /.all-categories -->
										<span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
									</div><!-- /.cat-wrap -->
									<div class="box-search">
										<input type="text" name="s" placeholder="Buscar Articulos" autocomplete="off">
										<span class="btn-search">
											<button type="submit" class="waves-effect"><img src="{{asset('images/icons/search.png')}}" alt=""></button>
										</span>
									</div><!-- /.box-search -->
								</form><!-- /.form-search -->
							</div><!-- /.top-search -->
						</div><!-- /.col-md-6 -->
						<div class="col-md-1">
							<div class="box-cart">
								<div class="inner-box">
									<ul class="menu-compare-wishlist">
										<li class="compare">
											<a href="{{route('TiendaComparacionFront')}}" title="">
												<img src="{{asset('images/icons/compare.png')}}" alt="">
												@if($comparar_count)
													<span>{{$comparar_count}}</span>
												@endif
											</a>
										</li>
									</ul><!-- /.menu-compare-wishlist -->
								</div><!-- /.inner-box -->
								<!--
								<div class="inner-box">
									<a href="#" title="">
										<div class="icon-cart">
											<img src="{{asset('images/icons/cart.png')}}" alt="">
											<span>{{count($carrito)}}</span>
										</div>
										<div class="price">
											$<i id="total_fuera_carrito"></i>
										</div>
									</a>
									<div class="dropdown-box">
										<ul>
											@php
												$total=0.00;
											@endphp
											@foreach($carrito as $producto)
												@php
													if(auth()->user()){
														$cantidad=$producto->pivot->cantidad;
													}else{
														$cantidad=$producto->cantidad;
													}
													$total=$total+($producto->precio*$cantidad);
												@endphp
												<li>
													<a href="{{route('TiendaArticuloFront',$producto['slug'])}}">
														<div class="img-product" style="
															background-image: url('{{asset($producto->foto)}}');
															background-size: cover;
														    background-repeat: no-repeat;
														">
														</div>
													</a>
													<div class="info-product">
														<div class="name">
															<a href="{{route('TiendaArticuloFront',$producto['slug'])}}">
																{{$producto->nombre}}
															</a>
														</div>
														<div class="price">
															<span id="cantidad_carrito_header_{{$producto->id}}">{{$cantidad}}</span>
															<span>x</span>
															<span>{{$producto->precio}}</span>
														</div>
													</div>
													<div class="clearfix"></div>
													<a href="{{route('TiendaCarritoEliminarFront',$producto->slug)}}">
														<span class="delete">x</span>
													</a>
												</li>
											@endforeach
										</ul>
										<div class="total">
											<span>Subtotal:</span>
											<span class="price">$<i id="total_final_pedido_header"></i>{{$total}}</span></span>
											<script type="text/javascript">
												document.getElementById('total_fuera_carrito').innerHTML = {{$total}};
											</script>
										</div>
										<div class="btn-cart">
											<a href="{{route('TiendaCarritoVerFront')}}" class="view-cart" title="">Ver Carrito</a>
											<a href="{{route('TiendaPedidoGenerarFront')}}" class="check-out" title="">Finalizar</a>
										</div>

									</div>
								</div>
								-->
							</div><!-- /.box-cart -->
						</div><!-- /.col-md-3 -->
					</div><!-- /.row -->
				</div><!-- /.container -->
			</div><!-- /.header-middle -->
			<div class="header-bottom">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-2">
							<div id="mega-menu">
								<div class="btn-mega"><span></span>CATEGORIAS</div>
								@foreach ($categorias as $categoria)
								    {!!$categoria!!}
							  	@endforeach
							</div>
						</div><!-- /.col-md-3 -->
						<div class="col-md-9 col-10">
							<div class="nav-wrap">
								<div id="mainnav" class="mainnav">
									<ul class="menu">
										<li class="column-1">
											<a href="{{route('home')}}" title="">Inicio</a>

										</li><!-- /.column-1 -->
										<li class="column-1">
											<a href="{{route('QuienesSomosFront')}}" title="">Quienes Somos</a>
										</li><!-- /.column-1 -->
										<li class="column-1">
											<a href="{{route('TiendaFront')}}" title="">Tienda</a>
										</li>
										<li class="column-1">
											<a href="{{route('GarantiaSoporteFront')}}" title="">Garantia y Soporte</a>
										</li>
										<li class="column-1">
											<a href="{{route('ClientesFront')}}" title="">Clientes</a>
										</li>
										<li class="column-1">
											<a href="{{route('ContactoFront')}}" title="">Contacto</a>
										</li>
									</ul><!-- /.menu -->
								</div><!-- /.mainnav -->
							</div><!-- /.nav-wrap -->
							<!-- <div class="today-deal">
								<a href="#" title="">TODAY DEALS</a>
							</div> -->
							<div class="btn-menu">
	                            <span></span>
	                        </div><!-- //mobile menu button -->
						</div><!-- /.col-md-9 -->
					</div><!-- /.row -->
				</div><!-- /.container -->
			</div><!-- /.header-bottom -->
		</section><!-- /#header -->

		@yield('contenido')
		<section class="flat-row flat-iconbox style3">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6">
						<div class="iconbox style1">
							<div class="box-header" style="background-image: url('{{asset('images/banner_nano/banner_1.jpg')}}'); height:88px;background-repeat: no-repeat;">

							</div><!-- /.box-header -->
						</div><!-- /.iconbox -->
					</div><!-- /.col-lg-3 col-md-6 -->
					<div class="col-lg-3 col-md-6">
						<div class="iconbox style1">
							<div class="box-header" style="background-image: url('{{asset('images/banner_nano/banner_2.jpg')}}'); height:88px;background-repeat: no-repeat;">

							</div><!-- /.box-header -->
						</div><!-- /.iconbox -->
					</div><!-- /.col-lg-3 col-md-6 -->
					<div class="col-lg-3 col-md-6">
						<div class="iconbox style1">
							<div class="box-header" style="background-image: url('{{asset('images/banner_nano/banner_3.jpg')}}'); height:88px;background-repeat: no-repeat;">

							</div><!-- /.box-header -->
						</div><!-- /.iconbox -->
					</div><!-- /.col-lg-3 col-md-6 -->
					<div class="col-lg-3 col-md-6">
						<div class="iconbox style1">
							<div class="box-header">
								<div class="image">
									<img src="{{asset('images/icons/return.png')}}" alt="">
								</div>
								<div class="box-title">
									<h3>Return 30 Days</h3>
								</div>
								<div class="clearfix"></div>
							</div><!-- /.box-header -->
						</div><!-- /.iconbox -->
					</div><!-- /.col-lg-3 col-md-6 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.flat-iconbox -->
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6">
						<div class="widget-ft widget-about">
							<div class="logo logo-ft">
								<a href="index.php" title="">
									<img src="{{asset('images/logos/logo.png')}}" alt="">
								</a>
							</div><!-- /.logo-ft -->
							<div class="widget-content">
								<div class="icon">
									<img src="{{asset('images/icons/phone.png')}}" alt="">
								</div>
								<div class="info">
									<p class="questions">Consultas?</p>
									<p class="phone">+54 11 4788-3174</p>
									<p class="address">
										Dirección Virrey del Pino 1540, CABA, Bs. As.,<br />Argentina.
									</p>
								</div>
							</div><!-- /.widget-content -->
							<ul class="social-list">
								<li>
									<a href="#" title="">
										<i class="fa fa-facebook" aria-hidden="true"></i>
									</a>
								</li>
								<li>
									<a href="#" title="">
										<i class="fa fa-twitter" aria-hidden="true"></i>
									</a>
								</li>
								<li>
									<a href="#" title="">
										<i class="fa fa-instagram" aria-hidden="true"></i>
									</a>
								</li>
								<li>
									<a href="#" title="">
										<i class="fa fa-pinterest" aria-hidden="true"></i>
									</a>
								</li>
								<li>
									<a href="#" title="">
										<i class="fa fa-dribbble" aria-hidden="true"></i>
									</a>
								</li>
								<li>
									<a href="#" title="">
										<i class="fa fa-google" aria-hidden="true"></i>
									</a>
								</li>
							</ul><!-- /.social-list -->
						</div><!-- /.widget-about -->
					</div><!-- /.col-lg-3 col-md-6 -->
					<div class="col-lg-9">
						<div style="height: 400px; width: 100%;" id="map"></div>
					</div><!-- /.col-lg-4 col-md-6 -->
				</div><!-- /.row -->
				<!-- <div class="row">
					<div class="col-md-12">
						<div class="widget widget-apps">
							<div class="widget-title">
								<h3>Mobile Apps</h3>
							</div>
							<ul class="app-list">
								<li class="app-store">
									<a href="#" title="">
										<div class="img">
											<img src="{{asset('images/icons/app-store.png')}}" alt="">
										</div>
										<div class="text">
											<h4>App Store</h4>
											<p>Available now on the</p>
										</div>
									</a>
								</li>
								<li class="google-play">
									<a href="#" title="">
										<div class="img">
											<img src="{{asset('images/icons/google-play.png')}}" alt="">
										</div>
										<div class="text">
											<h4>Google Play</h4>
											<p>Get in on</p>
										</div>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div> -->
			</div><!-- /.container -->
		</footer><!-- /footer -->

		<section class="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p class="copyright"> Todos Los Derechos Reservados © NanoTec S.R.L - Buenos Aires - Argentina</p>
						<p class="btn-scroll">
							<a href="#" title="">
								<img src="{{asset('images/icons/top.png')}}" alt="">
							</a>
						</p>
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.footer-bottom -->

	</div><!-- /.boxed -->

	<!-- Javascript -->
	<script type="text/javascript" src="{{ asset('webpageshop/javascript/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('webpageshop/javascript/tether.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('webpageshop/javascript/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('webpageshop/javascript/waypoints.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('webpageshop/javascript/easing.js') }}"></script>
	<script type="text/javascript" src="{{ asset('webpageshop/javascript/jquery.zoom.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('webpageshop/javascript/jquery.flexslider-min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('webpageshop/javascript/owl.carousel.js') }}"></script>

	<script type="text/javascript" src="{{ asset('webpageshop/javascript/jquery.mCustomScrollbar.js') }}"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtRmXKclfDp20TvfQnpgXSDPjut14x5wk&region=GB"></script>
   	<script type="text/javascript" src="{{ asset('webpageshop/javascript/gmap3.min.js') }}"></script>
   	<script type="text/javascript" src="{{ asset('webpageshop/javascript/waves.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('webpageshop/javascript/jquery.countdown.js') }}"></script>

	<script type="text/javascript" src="{{ asset('webpageshop/javascript/main.js') }}"></script>
	<script type="text/javascript">
	//Agregado 11-08-2017
	//Script para visualizar google Maps - Guillermo Balmaceda
	//Inicio de script
	function initMap() {
		var image = 'http://www.nanotec.com.ar/img/map/logoMaps.png';
		var uluru1 = {lat: -34.561036,  lng:  -58.442987};
		var uluru = {lat:  -34.561036 ,  lng:  -58.442987};
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 17,
			center: uluru
		});
		var marker = new google.maps.Marker({
			position: uluru1,
			map: map,
			icon: image,
			title: 'NanoTec SRL'
		});

		var contentString = '<div id="content" style="text-align: center;">'+
							'<h3 id="firstHeading" class="firstHeading">Nanotec SRL</h3>'+
							'<div id="bodyContent" class="hidden-lg" style="text-align: center;">'+
				'<button type="button" class="btn btn-primary btn-md" onClick="gotonano();">'+
					'<i class="fa fa-location-arrow" aria-hidden="true"></i> IR a NanoTec</button>'+
							'</div>'+
							'</div>';

		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});

		infowindow.open(map,marker);
	}
	google.maps.event.addListener(marker, 'click', function() {
		 infowindow.open(map,marker);
	});
	function gotonano(){
		window.open("google.navigation:q=-34.561036,-58.442987" , "_system");
	}
	//Fin Script
	</script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDUaqJq7_otsv6UWUs_hbVxkeXje8QczL4&callback=initMap"></script>
	</script>
	@yield('js')
</body>
</html>
