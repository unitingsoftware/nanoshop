<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
	<title>Garantia y Soporte - {{env('APP_NAME')}}</title>
</head>
<body>	
	<h3>Datos de Contacto:</h3>
	------------------------<br>
	<strong>Nombre del CLiente: </strong> {{$correo['name']}}<br>
	<strong>Persona de Contacto: </strong> {{$correo['contact_name']}}<br>
	<strong>Teléfono de Contacto: </strong> {{$correo['phone']}}<br>
	<strong>Email: </strong> {{$correo['email']}}<br>
	

	<h3>Datos del Equipo:</h3>
	------------------------<br>
	<strong>Marca del Producto: </strong> {{$correo['product_brand']}}<br>
	<strong>Número de contrato: </strong> {{$correo['numero']}}<br>
	<strong>Modelo del Producto: </strong> {{$correo['product_model']}}<br>
	<strong>Número de serie: </strong> {{$correo['product_sn']}}<br>
	<strong>Ubicación física del producto: </strong> {{$correo['fisical_loc']}}<br>
	<strong>Descripción de la falla: </strong> {{$correo['message']}}<br>

	<br>
</body>
</html>