<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
	<title>Nuevo Presupuesto - {{env('APP_NAME')}}</title>
</head>
<body>	
	<h1>Nueva Cotización Web </h1>

	<h3>Datos del cliente:</h3>
	------------------------<br>
	<strong>Nombre: </strong> {{$correo->name}}<br>
	<strong>Empresa: </strong> {{$correo->empresa}}<br>
	<strong>Correo: </strong> {{$correo->email}}<br>
	<strong>Telefono: </strong> {{$correo->telefono}}<br>

	<h3>Detalles del Articulo:</h3>
	------------------------<br>
	<strong>Articulo: </strong> <a href="{{route('TiendaArticuloFront',$correo->articulo->slug)}}">{{$correo->articulo->nombre}}</a><br>

	<br>
</body>
</html>