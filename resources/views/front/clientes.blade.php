@extends('layouts.webpageshop.app')
@section('contenido')
<style type="text/css">
	.slides img{
		max-height: 400px!important;
		max-width: 100%!important;
		width: auto!important;
	}
	.flex-control-thumbs {
	    margin: 30px 0 0!important;
	}
	.box-cart.style2 .btn-add-cart a {
	    width: 250px;
	}

	.style2  a:hover {
	    color: #fff!important;
	    background-color:#0153a6;
	}

	.style2  a {
	    color: #fff!important;
	    background-color:#484848;
	}

	.btn-add-cart  {
		display:inline-block;
		height:55px;
		line-height:55px;
		text-align:center;
		color:#fff;
		background-color:#0153a6;
		border-radius:30px;
		width:100%;
		font-size:16px;
		font-weight:600;
	}

	.btn-add-cart:hover {
		background-color:#2d2d2d;
		
	}

	.btn-add-cart img {
		padding-right:18px;
	}
</style>
<div class="boxed">
		<section class="flat-breadcrumb">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="breadcrumbs">
							<li class="trail-item">
								<a href="{{route('home')}} title="">Inicio</a>
								<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
							</li>
							<li class="trail-end">
								<a href="#" title="">Clientes</a>
							</li>
						</ul><!-- /.breacrumbs -->
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.flat-breadcrumb -->

		<section class="flat-imagebox style4">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="owl-carousel-3">

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente1.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente2.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente3.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente4.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente5.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente6.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente7.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente8.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente9.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente10.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente11.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente12.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente13.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente14.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente15.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente16.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente17.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente18.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente19.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente20.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente21.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente22.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente23.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente24.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente25.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente26.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente27.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente28.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente29.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente30.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente31.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente32.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente33.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente34.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente35.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente36.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente37.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente38.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente39.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente40.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente41.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente42.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente43.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente44.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente45.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente46.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente47.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente48.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente49.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente50.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente51.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente52.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente53.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente54.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente55.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente56.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente57.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente58.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente59.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente60.jpg')}}" alt="">
									</a>
								</div>
							</div>

							<div class="imagebox style4 blue">
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente61.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente62.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente63.jpg')}}" alt="">
									</a>
								</div>
								<div class="box-image">
									<a href="#" title="">
										<img src="{{asset('clientes/cliente64.jpg')}}" alt="">
									</a>
								</div>
							</div>


						</div><!-- /.owl-carousel-3 -->
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.flat-imagebox style4 -->

	</div>
@endsection

@section('js')

@endsection