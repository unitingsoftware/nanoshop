@extends('layouts.webpageshop.app')
@section('contenido')
<style type="text/css">
	.slides img{
		max-height: 400px!important;
		max-width: 100%!important;
		width: auto!important;
	}
	.flex-control-thumbs {
	    margin: 30px 0 0!important;
	}
	.box-cart.style2 .btn-add-cart a {
	    width: 250px;
	}

	.style2  a:hover {
	    color: #fff!important;
	    background-color:#0153a6;
	}

	.style2  a {
	    color: #fff!important;
	    background-color:#484848;
	}

	.btn-add-cart  {
		display:inline-block;
		height:55px;
		line-height:55px;
		text-align:center;
		color:#fff;
		background-color:#0153a6;
		border-radius:30px;
		width:100%;
		font-size:16px;
		font-weight:600;
	}

	.btn-add-cart:hover {
		background-color:#2d2d2d;
		
	}

	.btn-add-cart img {
		padding-right:18px;
	}
</style>
<div class="boxed">
		<section class="flat-breadcrumb">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="breadcrumbs">
							<li class="trail-item">
								<a href="{{route('home')}} title="">Inicio</a>
								<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
							</li>
							<li class="trail-item">
								<a href="{{route('TiendaFront')}}" title="">Tienda</a>
								<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
							</li>
							<li class="trail-item">
								<a href="{{route('TiendaCategoriaFront',array($articulo->categoria->id,$articulo->categoria->nombre))}}" title="">{{$articulo->categoria->nombre}}</a>
								<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
							</li>
							<li class="trail-end">
								<a href="#" title="">{{$articulo->nombre}}</a>
							</li>
						</ul><!-- /.breacrumbs -->
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.flat-breadcrumb -->

		<section class="flat-product-detail">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="flexslider">
							<ul class="slides">
							    <li data-thumb="{{str_replace('/files/', '/.thumbs/files/', $articulo->foto)}}">
							      <a href='#' id="zoom" class='zoom'>
							      	<img src="{{$articulo->foto}}" alt='' width='400' height='300' />
							      </a>
							    </li>
							    @php 
							    	$galeria=explode(';', $articulo->galeria);
							    @endphp
							    @for ($i=0; $i < count($galeria)-1; $i++)
								    <li data-thumb="{{str_replace('/files/', '/.thumbs/files/', $galeria[$i])}}">
								      <a href='#' id="zoom1" class='zoom'><img src="{{$galeria[$i]}}" alt='' width='400' height='300' /></a>
								    </li>
							    @endfor
							</ul><!-- /.slides -->
						</div><!-- /.flexslider -->
					</div><!-- /.col-md-6 -->
					<div class="col-md-6">
						<div class="product-detail">
							<div class="header-detail">
								<h4 class="name">{{$articulo->nombre}}</h4>
								<div class="category">
									{{$articulo->categoria->nombre}}
								</div>

							</div><!-- /.header-detail -->
							<div class="content-detail">
								<div class="price">
									<div class="sale">
										${{$articulo->precio}}
									</div>
								</div>
								<div class="info-text">
									

								</div>
							</div>
							<div class="footer-detail">
								<form action="{{route('TiendaPresupuestoArticuloFront',$articulo->slug)}}" method="POST">
									{{  csrf_field() }}
									<div class="quanlity-box">
										<div class="quanlity" style="width: 100%;">
											<input type="text"  name="name" value="" placeholder="Nombre" style="width: 100%;" required>
										</div>
									</div>
									<br>
									
									<div class="quanlity-box">
										<div class="quanlity" style="width: 100%;">
											<input type="text"  name="empresa" value="" placeholder="Empresa" style="width: 100%;" required>
										</div>
									</div>
									<br>
									
									<div class="quanlity-box">
										<div class="quanlity" style="width: 100%;">
											<input type="email"  name="email" value="" placeholder="Correo Electronico" style="width: 100%;" required>
										</div>
									</div>

									<div class="quanlity-box">
										<div class="quanlity" style="width: 100%;">
											<input type="tel"  name="telefono" value="" placeholder="Telefono" style="width: 100%;" required>
											<input type="hidden" readonly="" value="1" name="cantidad">
										</div>
									</div>
									<br>
									<div class="col-md-12">
										<div class="col-md-6" style="float: left!important;  margin: 0px!important;">
											<div class="box-cart style2">
												<a href="{{route('TiendaCompararAgregarFront',$articulo->slug)}}" class="btn-add-cart">
													<img src="{{asset('images/icons/compare-2.png')}}" alt="">Comparar
												</a>
											</div><!-- /.box-cart -->
										</div>
										<div class="col-md-6" style="float: left!important;  margin: 0px!important;">
											<div class="box-cart style2">
												<button type="submit" class="btn-add-cart">
													<img src="{{asset('images/icons/mail-3.png')}}" alt="">Solicitar
												</button>
											</div><!-- /.box-cart -->
										</div>
									</div>
								</form>
								<div class="social-single">
									<span>Compartir</span>
									<ul class="social-list">
										<li>
											<a href="#" title="">
												<i class="fa fa-facebook" aria-hidden="true"></i>
											</a>
										</li>
										<li>
											<a href="#" title="">
												<i class="fa fa-twitter" aria-hidden="true"></i>
											</a>
										</li>
										<li>
											<a href="#" title="">
												<i class="fa fa-instagram" aria-hidden="true"></i>
											</a>
										</li>
										<li>
											<a href="#" title="">
												<i class="fa fa-pinterest" aria-hidden="true"></i>
											</a>
										</li>
										<li>
											<a href="#" title="">
												<i class="fa fa-dribbble" aria-hidden="true"></i>
											</a>
										</li>
										<li>
											<a href="#" title="">
												<i class="fa fa-google" aria-hidden="true"></i>
											</a>
										</li>
									</ul><!-- /.social-list -->
								</div><!-- /.social-single -->
							</div><!-- /.footer-detail -->
						</div><!-- /.product-detail -->
					</div><!-- /.col-md-6 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.flat-product-detail -->
		 <section class="flat-product-content">
      <ul class="product-detail-bar">
        <li class="active">Description</li>
        <li>Caracteristicas Tecnicas</li>
      </ul><!-- /.product-detail-bar -->
      <div class="container">
        <div class="row">
          {{$articulo->descripcion}}
        </div><!-- /.row -->
        <div class="row">
          <div class="col-md-12">
            <div class="tecnical-specs">
              <h4 class="name">
                {{$articulo->nombre}}
              </h4>
              <table>
                <tbody>
                  @foreach($articulo->atributos_articulo as $atributo)
                    <tr>
                      <td>{{$atributo->nombre}}</td>
                      <td>{{str_replace('|_|',' ',$atributo->pivot->contenido)}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
	</div>
@endsection
@section('js')
	<script type="text/javascript">
		function SumarCantidad(id){
			let num = document.getElementById('cantidad_articulo').value; 
			document.getElementById('cantidad_articulo').value = parseInt(num)+1; 
		}

		function RestarCantidad(id){
			let num = document.getElementById('cantidad_articulo').value; 
			if (num > 1) {
				document.getElementById('cantidad_articulo').value = parseInt(num)-1; 
			}
		}
	</script>
@endsection