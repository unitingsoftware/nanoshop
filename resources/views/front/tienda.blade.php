@extends('layouts.webpageshop.app')
@section('contenido')
<div class="boxed">

		<section class="flat-breadcrumb">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="breadcrumbs">
							<li class="trail-item">
								<a href="#" title="">Inicio</a>
								<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
							</li>
							<li class="trail-item">
								<a href="#" title="">Tienda</a>
								<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
							</li>
							<li class="trail-end">
								<a href="#" title="">{{$titulo_categoria}}</a>
							</li>
						</ul><!-- /.breacrumbs -->
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.flat-breadcrumb -->

		<main id="shop">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-4">
						<div class="sidebar ">
							<div class="widget widget-categories">
								<div class="widget-title">
									<h3>{{$titulo_categoria}}<span></span></h3>
								</div>
								@foreach($menu_categoria as $menu)
									{!!$menu!!}
								@endforeach
							</div>
							<div class="widget widget-brands">
								<form method="GET" action="">
									@if(isset($filtros))
										@foreach ($filtros AS $filtro)
											<div class="widget-title">
												<h3>{{$filtro['nombre']}}<span></span></h3>
											</div>
											<div class="widget-content">
												<ul class="box-checkbox scroll">
													<?php $i=0;  ?>
													@foreach($filtro['contenido'] AS $key => $atributo)
														@if($atributo['cantidad'])
															@if(in_array($atributo['nombre'],$filtro_actual))
																<li class="check-box">
																	<input type="checkbox" id="checkbox{{$key}}" name="filtro[{{$i}}]" checked value="{{$atributo['nombre']}}" onclick="this.form.submit();">
																	<label for="checkbox{{$key}}">{{$atributo['nombre']}} <span>({{$atributo['cantidad']}})</span></label>
																</li>
															@else
																<li class="check-box">
																	<input type="checkbox" id="checkbox{{$key}}" name="filtro[{{$i}}]" value="{{$atributo['nombre']}}" onclick="this.form.submit();">
																	<label for="checkbox{{$key}}">{{$atributo['nombre']}} <span>({{$atributo['cantidad']}})</span></label>
																</li>
															@endif
															<?php $i++; ?>
														@endif
													@endforeach
												</ul>
											</div>
										@endforeach
									@endif
								</form>
<<<<<<< HEAD
							</div>
						</div>
					</div>
=======
							</div><!-- /.widget widget-brands -->

						</div><!-- /.sidebar -->
					</div><!-- /.col-lg-3 col-md-4 -->
>>>>>>> master
					<div class="col-lg-9 col-md-8">
						<div class="main-shop">

							<div class="wrap-imagebox">
								<div class="tab-product">
									<div class="row sort-box">
										@foreach($articulos as $articulo)
											<div class="col-lg-4 col-sm-6">
												<div class="product-box">
													<div class="imagebox">
														<div class="box-image">
															<a href="{{route('TiendaArticuloFront',$articulo->slug)}}" title="">
																<img src="{{$articulo->foto}}" alt="">
															</a>
														</div><!-- /.box-image -->
														<div class="box-content">
															<div class="cat-name">
																<a href="{{route('TiendaCategoriaFront',array($articulo->categoria->id,$articulo->categoria->nombre))}}">{{$articulo->categoria->nombre}}</a>
															</div>
															<div class="product-name">
																<a href="{{route('TiendaArticuloFront',$articulo->slug)}}" title="">{{$articulo->nombre}}</a>
															</div>
															<div class="price">
																<span class="sale">${{$articulo->precio}}</span>
															</div>
														</div><!-- /.box-content -->
														<div class="box-bottom">
															<div class="btn-add-cart">
																<a href="{{route('TiendaArticuloFront',$articulo->slug)}}" title="">
																	<img src="{{asset('images/icons/mail-3.png')}}" alt="">Más Información
																</a>
															</div>
															<div class="compare-wishlist">
																<a href="{{route('TiendaCompararAgregarFront',$articulo->slug)}}" class="compare" title="">
																	<img src="{{asset('images/icons/compare.png')}}" alt="">Comparar
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										@endforeach
									</div>
								</div>
							</div>
							<div class="blog-pagination">
							    {!!$articulos->render()!!}
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>

	<script type="text/javascript">

	</script>
@endsection
