@extends('layouts.webpageshop.app')
@section('contenido')
<style type="text/css">
	.slides img{
		max-height: 400px!important;
		max-width: 100%!important;
		width: auto!important;
	}
	.flex-control-thumbs {
	    margin: 30px 0 0!important;
	}
	.box-cart.style2 .btn-add-cart a {
	    width: 250px;
	}

	.style2  a:hover {
	    color: #fff!important;
	    background-color:#0153a6;
	}

	.style2  a {
	    color: #fff!important;
	    background-color:#484848;
	}

	.btn-add-cart  {
		display:inline-block;
		height:55px;
		line-height:55px;
		text-align:center;
		color:#fff;
		background-color:#0153a6;
		border-radius:30px;
		width:100%;
		font-size:16px;
		font-weight:600;
	}

	.btn-add-cart:hover {
		background-color:#2d2d2d;
		
	}

	.btn-add-cart img {
		padding-right:18px;
	}
</style>
<div class="boxed">
		<section class="flat-breadcrumb">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="breadcrumbs">
							<li class="trail-item">
								<a href="{{route('home')}} title="">Inicio</a>
								<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
							</li>
							<li class="trail-end">
								<a href="#" title="">Garantia y Soporte</a>
							</li>
						</ul><!-- /.breacrumbs -->
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.flat-breadcrumb -->

		<section class="flat-product-detail">
			<div class="container">
			    <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <div class="">
                        	<p>En Nanotec SRL nos preocupamos por estar cerca de nuestros clientes en todo lo relacionado con soporte post-venta y garantía de nuestros productos.</p>
							<h3><i class="fa fa-envelope-o"></i> Envíe su consulta</h3>
							<hr>
							<div class="row">
								<form action="{{route('GarantiaSoporteCorreoFront')}}" method="post" style="width: 100%;">
									
									@csrf
									<div class="col-sm-6">
                                        <div class="form-group">
	                                        <label>Nombre del cliente *</label>
											<input name="name" id="name" type="text" placeholder="Nombre del cliente">
										</div>
									</div>
									<div class="col-sm-6">
                                        <div class="form-group">
	                                        <label>Marca del Producto *</label>
											<input name="product_brand" id="product_brand" type="text" placeholder="Marca del Producto">
										</div>
									</div>
									<div class="col-sm-6">
                                        <div class="form-group">
	                                        <label>Número de contrato</label>
											<input name="numero" id="numero" type="text" placeholder="Número de contrato">
										</div>
									</div>
									<div class="col-sm-6">
                                        <div class="form-group">
	                                        <label>Modelo del Producto *</label>
											<input name="product_model" id="product_model" type="text" placeholder="Modelo del Producto">
										</div>
									</div>
									<div class="col-sm-6">
                                        <div class="form-group">
	                                        <label>Persona de contacto *</label>
											<input name="contact_name" id="contact_name" type="text" placeholder="Persona de contacto">
										</div>
									</div>
									<div class="col-sm-6">
                                        <div class="form-group">
	                                        <label>Número de serie *</label>
											<input name="product_sn" id="product_sn" type="text" placeholder="Número de serie">
										</div>
									</div>
									<div class="col-sm-6">
                                        <div class="form-group">
	                                        <label>Email *</label>
											<input name="email" id="email" type="email" placeholder="ejemplo@ejemplo.com">
										</div>
									</div>
									<div class="col-sm-6">
                                        <div class="form-group">
	                                        <label>Ubicación física del producto</label>
											<input name="fisical_loc" id="fisical_loc" type="text" placeholder="Ubicación física del producto">
										</div>
									</div>
									<div class="col-sm-12">
                                        <div class="form-group">
	                                        <label>Teléfono de contacto</label>
											<input name="phone" id="phone" type="text" placeholder="000 0000-0000">
										</div>
									</div>
									<div class="col-sm-12">
                                        <div class="form-group">
	                                        <label>Descripción de la falla *</label>
											<textarea name="message" placeholder="Mensaje"></textarea>
										</div>
									</div>
									<div class="col-sm-12">
                                        <div class="form-group">
											<input type="submit" href="#" class="pull-right" value="Enviar" style="color:#fff;">
										</div>
									</div>
								</form>
							</div>
						</div>
                    </div>
                </div>
            </div>
		</section><!-- /.flat-product-detail -->
	</div>
@endsection

@section('js')

@endsection