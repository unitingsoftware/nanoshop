@extends('layouts.webpageshop.app')
@section('contenido')
<div class="box-checkout">
	<section class="flat-breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumbs">
						<li class="trail-item">
							<a href="#" title="">Inicio</a>
							<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
						</li>
						<li class="trail-item">
							<a href="{{route('TiendaMiCuentaFront')}}" title="">Usuarios</a>
							<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
						</li>
						<li class="trail-end">
							<a href="#" title="">Nuevo Pedido</a>
						</li>
					</ul><!-- /.breacrumbs -->
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.flat-breadcrumb -->

	<section class="flat-checkout">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<div class="box-checkout">
						<form action="#" method="get" class="checkout" accept-charset="utf-8">
							<div class="billing-fields">
								<div class="fields-title">
									<h3>Detalles del pedido</h3>
									<div class="clearfix"></div>
								</div><!-- /.fields-title -->
								<div class="fields-content">
									<div class="field-row">
										<label for="first-name">Nombre *</label>
										<input type="text" readonly="" value="{{auth()->user()->name}}" placeholder="Ali">
										<div class="clearfix"></div>
									</div>
									<div class="field-row">
										<label for="first-name">Usuario *</label>
										<input type="text" readonly="" value="{{auth()->user()->user}}"ireadonly="" value="{{auth()->user()->name}}" placeholder="Ali">
										<div class="clearfix"></div>
									</div>
									<div class="field-row">
										<p class="field-one-half">
											<label for="email-address">Correo Electonico *</label>
											<input type="email" readonly="" value="{{auth()->user()->email}}">
										</p>
										<p class="field-one-half">
											<label for="phone">Telefono *</label>
											<input type="text" readonly="" value="{{auth()->user()->telefono}}">
										</p>
										<div class="clearfix"></div>
									</div>
								</div><!-- /.fields-content -->
							</div><!-- /.billing-fields -->
							
						</form><!-- /.checkout -->
					</div><!-- /.box-checkout -->
				</div><!-- /.col-md-7 -->
				<div class="col-md-5">
					<div class="cart-totals style2">
						<h3>Resumen</h3>
						<form action="#" method="get" accept-charset="utf-8">
							<table class="product">
								<thead>
									<tr>
										<th>Productos</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									@php
										$total=0;
									@endphp
									@foreach($carrito as $articulo)
										<tr>
											<td>{{$articulo->nombre}}</td>
											<td>${{$articulo->precio}} X {{$articulo->pivot->cantidad}}</td>
										</tr>
										@php
											$precio=$articulo->precio*$articulo->pivot->cantidad;
											$total=$total+$precio;
										@endphp
									@endforeach
								</tbody>
							</table><!-- /.product -->
							<table>
								<tbody>
									<tr>
										<td>Total</td>
										<td class="price-total">${{$total}}</td>
									</tr>
								</tbody>
							</table>
							
							<div class="checkbox">
								<input type="checkbox" id="checked-order" name="checked-order" checked>
								<label for="checked-order">He leído y acepto los términos y condiciones *</label>
							</div><!-- /.checkbox -->
							<div class="btn-order">
								<a href="{{route('TiendaPedidoEjecutarFront')}}" class="order" title="">Solicitar Pedido</a>
							</div><!-- /.btn-order -->
						</form>
					</div><!-- /.cart-totals style2 -->
				</div><!-- /.col-md-5 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.flat-checkout -->
</div>
	
@endsection

@section('js')
	<script type="text/javascript">
		function SumarCantidad(lugar,id){
			let num = document.getElementById('cantidad'+lugar).value; 
			document.getElementById('cantidad'+lugar).value = parseInt(num)+1; 
			ActualizarCantidades(lugar,id);
		}

		function RestarCantidad(lugar,id){
			let num = document.getElementById('cantidad'+lugar).value; 
			if (num > 1) {
				document.getElementById('cantidad'+lugar).value = parseInt(num)-1; 
			}
			ActualizarCantidades(lugar,id);
		}
		function ActualizarCantidades(lugar,id){
		    var cantidad = $('#cantidad'+lugar).val();
		    var precio = $('#precio'+lugar).val();
		    $.ajax({
		        type:'GET',
		        url:'{{route("TiendaCarritoActualizarAjaxFront")}}',
		        dataType: "json",
		        data:{id:id,cantidad:cantidad},
		        success:function(data){
		        	document.getElementById('cantidad_carrito_header_'+id).innerHTML = cantidad; 
		        	document.getElementById('total_articulo'+id).innerHTML = parseInt(cantidad*precio); 
		        	document.getElementById('total_final_pedido').innerHTML = data; 
		        	document.getElementById('total_final_pedido_header').innerHTML = data; 
		        	document.getElementById('total_fuera_carrito').innerHTML = data; 
		        }
		    });
		  }
	</script>
@endsection