@extends('layouts.webpageshop.app')
@section('contenido')

<section class="flat-breadcrumb">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumbs">
					<li class="trail-item">
						<a href="{{route('home')}}" title="">Inicio</a>
						<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
					</li>
					<li class="trail-item">
						<a href="{{route('TiendaMiCuentaFront')}}" title="">Mi Cuenta</a>
						<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
					</li>
					<li class="trail-item">
						<a href="{{route('TiendaPedidoListarFront')}}" title="">Mis Pedidos</a>
						<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
					</li>
					<li class="trail-end">
						<a href="#" title="">Pedido ID #{{$pedido->id}}</a>
					</li>
				</ul><!-- /.breacrumbs -->
			</div><!-- /.col-md-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.flat-breadcrumb -->

<section class="flat-tracking background">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="order-tracking">
					<div class="title">
						<h3>Detalles del Pedido</h3>
					</div>
					<div class="table-cart">
						<table>
							<thead>
								<tr>
									<th>Articulo</th>
									<th>Precio</th>
									<th>Cantidad</th>
									<th>Totales</th>
								</tr>
							</thead>
							<tbody>
								@php
									$total=0;
									$i=1;
								@endphp
								@foreach($pedido->articulos_pedido as $key => $articulo)
									<tr>
										<td>
											<div class="img-product">
												<a href="{{route('TiendaArticuloFront',$articulo->slug)}}">
													<img src="{{$articulo->foto}}" alt="">
												</a>
											</div>
											<div class="name-product">
												<a href="{{route('TiendaArticuloFront',$articulo->slug)}}">{{$articulo->nombre}}</a>
											</div>
											<div class="clearfix"></div>
										</td>
										<td>
											<div class="price" >
												${{$articulo->pivot->precio}}
											</div>
										</td>
										<td>
											{{$articulo->pivot->cantidad}}
										</td>
										<td>
											<div class="total" id="total_articulo{{$articulo->id}}">
												@php
													$i++;
													$precio=$articulo->pivot->precio*$articulo->pivot->cantidad;
													$total=$total+$precio;
												@endphp
												${{$precio}}
											</div>
										</td>
									</tr>
								@endforeach
								@section('precio_carrito')
									{{$total}}
								@endsection
							</tbody>
							<tfoot>
								<td colspan="3">
									<div class="total" id="total_articulo{{$articulo->id}}">Total:</div>
								</td>
								<td>
									<div class="total" id="total_articulo{{$articulo->id}}">$<strong>{{$total}}</strong></div>
								</td>
							</tfoot>
						</table>
					</div><!-- /.tracking-content -->
					<div class="title">
						<h3>Estado: <strong>{{$pedido->estado}}</strong></h3>
					</div>
				</div><!-- /.order-tracking -->
			</div><!-- /.col-md-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.flat-tracking -->

@endsection