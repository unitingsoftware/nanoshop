@extends('layouts.webpageshop.app')
@section('contenido')

<section class="flat-breadcrumb">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumbs">
					<li class="trail-item">
						<a href="{{route('home')}}" title="">Inicio</a>
						<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
					</li>
					<li class="trail-item">
						<a href="{{route('TiendaMiCuentaFront')}}" title="">Mi Cuenta</a>
						<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
					</li>
					<li class="trail-end">
						<a href="#" title="">Mis Pedidos</a>
					</li>
				</ul><!-- /.breacrumbs -->
			</div><!-- /.col-md-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.flat-breadcrumb -->

<section class="flat-tracking background">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="order-tracking">
					<div class="title">
						<h3>Mis Pedidos</h3>
					</div>
					<div class="wishlist-content">
						<table class="table-wishlist">
							<thead>
								<tr>
									<th>Información del Pedido</th>
									<th>Total</th>
									<th>Estado</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($pedidos as $key => $pedido)
									<tr>
										<td style="text-align: left;">
											<div class="delete">
												<a href="#" title="">#{{$key+1}})</a>
											</div>
												
											
											<div class="product">
												<div class="name">
													Pedido #{{$pedido->id}}<br>
													Numero de Articulos: {{$pedido->cantidad}}
												</div>
											</div>
											<div class="clearfix"></div>
										</td> 

										<td style="text-align: left;">
											<div class="price" id="">
												${{$pedido->total}}
											</div>
										</td>
										<td>
											<div class="status-product">
												<span>{{$pedido->estado}}</span>
											</div>
												
										</td>
										<td>
											<div class="add-cart">
												<a href="{{route('TiendaPedidoVerFront',$pedido->id)}}" title="">
													<img src="{{asset('images/icons/add-cart.png')}}" alt="">Ver Pedido
												</a>
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div><!-- /.tracking-content -->
				</div><!-- /.order-tracking -->
			</div><!-- /.col-md-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.flat-tracking -->


@endsection