@extends('layouts.webpageshop.app')
@section('contenido')
<div class="boxed">
	
<section class="flat-breadcrumb">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumbs">
					<li class="trail-item">
						<a href="{{route('home')}}" title="">Inicio</a>
						<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
					</li>
					<li class="trail-end">
						<a href="#" title="">Mi Cuenta</a>
					</li>
				</ul><!-- /.breacrumbs -->
			</div><!-- /.col-md-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.flat-breadcrumb -->

<section class="flat-compare">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="wrap-compare">
					<div class="title">
						<h3>Mi Cuenta</h3>
					</div>
					<div class="compare-content">
						<table class="table-compare" width="100%">
							<thead>
								<tr>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<form action="" method="POST">
										<th>Informacion</th>
										<td class="product">
												Nombre:<br>
											{{$usuario->name}}<br><br>
												Usuario:<br>
											{{$usuario->user}}
										</td><!-- /.product -->
										<td class="product">
											Correo:<br>
											{{$usuario->email}}
										</td><!-- /.product -->
										<td class="product">
											Telefono:<br>
											{{$usuario->telefono}}
										</td><!-- /.product -->
									</form>
								</tr>
								<tr>
									<th>Accioness</th>
									<td class="add-cart">
										<a href="{{route('TiendaCarritoVerFront')}}" title=""><img src="{{asset('images/icons/add-cart.png')}}" alt="">Ver Carrito</a>
										
									</td><!-- /.add-cart -->
									<td class="add-cart">
										<a href="{{route('TiendaFront')}}" title=""><img src="{{asset('images/icons/add-cart.png')}}" alt="">Ir a la Tienda</a>
										
									</td><!-- /.add-cart -->
									<td class="add-cart">
										<a href="{{route('TiendaPedidoListarFront')}}" title=""><img src="{{asset('images/icons/add-cart.png')}}" alt="">Pedidos</a>
									</td><!-- /.add-cart -->
								</tr>
							</tbody>
						</table><!-- /.table-compare -->
					</div><!-- /.compare-content -->
				</div><!-- /.wrap-compare -->
			</div><!-- /.col-md-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.flat-compare -->

<section class="flat-row flat-iconbox style1 background">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="iconbox style1 v1">
					<div class="box-header">
						<div class="image">
							<img src="{{asset('images/icons/car.png')}}" alt="">
						</div>
						<div class="box-title">
							<h3>Worldwide Shipping</h3>
						</div>
						<div class="clearfix"></div>
					</div><!-- /.box-header -->
				</div><!-- /.iconbox -->
			</div><!-- /.col-md-3 -->
			<div class="col-md-3">
				<div class="iconbox style1 v1">
					<div class="box-header">
						<div class="image">
							<img src="{{asset('images/icons/order.png')}}" alt="">
						</div>
						<div class="box-title">
							<h3>Order Online Service</h3>
						</div>
						<div class="clearfix"></div>
					</div><!-- /.box-header -->
				</div><!-- /.iconbox -->
			</div><!-- /.col-md-3 -->
			<div class="col-md-3">
				<div class="iconbox style1 v1">
					<div class="box-header">
						<div class="image">
							<img src="{{asset('images/icons/payment.png')}}" alt="">
						</div>
						<div class="box-title">
							<h3>Payment</h3>
						</div>
						<div class="clearfix"></div>
					</div><!-- /.box-header -->
				</div><!-- /.iconbox -->
			</div><!-- /.col-md-3 -->
			<div class="col-md-3">
				<div class="iconbox style1 v1">
					<div class="box-header">
						<div class="image">
							<img src="{{asset('images/icons/return.png')}}" alt="">
						</div>
						<div class="box-title">
							<h3>Return 30 Days</h3>
						</div>
						<div class="clearfix"></div>
					</div><!-- /.box-header -->
				</div><!-- /.iconbox -->
			</div><!-- /.col-md-3 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.flat-iconbox -->
</div>
@endsection