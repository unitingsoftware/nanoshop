@extends('layouts.webpageshop.app')
@section('contenido')
<div class="boxed">

	<section class="flat-breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumbs">
						<li class="trail-item">
							<a href="{{route('home')}}" title="">Inicio</a>
							<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
						</li>
						<li class="trail-item">
							<a href="{{route('TiendaMiCuentaFront')}}" title="">Usuarios</a>
							<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
						</li>
						<li class="trail-end">
							<a href="{{route('TiendaUsuariosLoginFront')}}" title="">Login</a>
						</li>
					</ul><!-- /.breacrumbs -->
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.flat-breadcrumb -->

	<section class="flat-account background">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="form-login">
						<div class="title">
							<h3>Iniciar Sesión </h3>
						</div>
						<form action="{{ route('login') }}" method="POST" id="form-register" accept-charset="utf-8">
							{{  csrf_field() }}
							<div class="form-box">
								@if ($errors->has('user'))
						            <span class="help-block">
						                <strong>{{ $errors->first('user') }}</strong>
						            </span>
						        @endif
								<label for="name-login">Usuario * </label>
								<input type="text" id="name-login" name="user" placeholder="Ali">
							</div><!-- /.form-box -->
							<div class="form-box">
								@if ($errors->has('password'))
						            <span class="help-block">
						                <strong>{{ $errors->first('password') }}</strong>
						            </span>
						        @endif
								<label for="password-login">Contraseña * </label>
								<input type="password" id="password-login" name="password" placeholder="******">
							</div><!-- /.form-box -->
							<div class="form-box checkbox">
								<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> 
								<label for="remember">Recordar Cuenta</label>
							</div><!-- /.form-box -->
							<div class="form-box">
								<button type="submit" class="login">Entrar</button>
								<a href="#" title="">Perdio su contraseña?</a>
							</div><!-- /.form-box -->
						</form><!-- /#form-login -->
					</div><!-- /.form-login -->
				</div><!-- /.col-md-6 -->
				<div class="col-md-6">
					<div class="form-register">
						<div class="title">
							<h3>Registro</h3>
						</div>
						<form action="{{ route('register') }}" method="POST" id="form-login" accept-charset="utf-8">
							{{  csrf_field() }}
							<div class="form-box has-feedback {{ $errors->has('name') ? ' has-error' : '' }}">
						        <input type="text"  name="name" placeholder="Nombre Completo" required>
						        @if ($errors->has('name'))
						            <span class="help-block">
						                <strong>{{ $errors->first('name') }}</strong>
						            </span>
						        @endif
					      	</div>
					      	<div class="form-box has-feedback {{ $errors->has('user') ? ' has-error' : '' }}">
						        <input type="text"  name="user" placeholder="User" required>
						        @if ($errors->has('user'))
						            <span class="help-block">
						                <strong>{{ $errors->first('user') }}</strong>
						            </span>
						        @endif
					      	</div>
					      	<div class="form-box has-feedback {{ $errors->has('telefono') ? ' has-error' : '' }}">
						        <input type="text"  name="telefono" placeholder="Telefono" required>
						        @if ($errors->has('telefono'))
						            <span class="help-block">
						                <strong>{{ $errors->first('telefono') }}</strong>
						            </span>
						        @endif
					      	</div>
					      	<div class="form-box has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
						        <input type="email"  name="email" placeholder="email" required>
						        @if ($errors->has('email'))
						            <span class="help-block">
						                <strong>{{ $errors->first('email') }}</strong>
						            </span>
						        @endif
					      	</div>
					      	<div class="form-box has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
						        <input type="password" name="password" placeholder="Password" required>
						        @if ($errors->has('password'))
						            <span class="help-block">
						                <strong>{{ $errors->first('password') }}</strong>
						            </span>
						        @endif
					      	</div>
					      	<div class="form-box has-feedback">
						        <input type="password" id="password-confirm"   name="password_confirmation" required placeholder="Confirme password">
					      	</div>

							<div class="form-box">
								<button type="submit" class="register">Register</button>
							</div><!-- /.form-box -->
						</form><!-- /#form-register -->
					</div><!-- /.form-register -->
				</div><!-- /.col-md-6 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.flat-account -->

	<section class="flat-row flat-iconbox style1 background">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-3">
					<div class="iconbox style1 v1">
						<div class="box-header">
							<div class="image">
								<img src="{{asset('images/icons/car.png')}}" alt="">
							</div>
							<div class="box-title">
								<h3>Worldwide Shipping</h3>
							</div>
							<div class="clearfix"></div>
						</div><!-- /.box-header -->
					</div><!-- /.iconbox -->
				</div><!-- /.col-md-6 col-lg-3 -->
				<div class="col-md-6 col-lg-3">
					<div class="iconbox style1 v1">
						<div class="box-header">
							<div class="image">
								<img src="{{asset('images/icons/order.png')}}" alt="">
							</div>
							<div class="box-title">
								<h3>Order Online Service</h3>
							</div>
							<div class="clearfix"></div>
						</div><!-- /.box-header -->
					</div><!-- /.iconbox -->
				</div><!-- /.col-md-6 col-lg-3 -->
				<div class="col-md-6 col-lg-3">
					<div class="iconbox style1 v1">
						<div class="box-header">
							<div class="image">
								<img src="{{asset('images/icons/payment.png')}}" alt="">
							</div>
							<div class="box-title">
								<h3>Payment</h3>
							</div>
							<div class="clearfix"></div>
						</div><!-- /.box-header -->
					</div><!-- /.iconbox -->
				</div><!-- /.col-md-6 col-lg-3 -->
				<div class="col-md-6 col-lg-3">
					<div class="iconbox style1 v1">
						<div class="box-header">
							<div class="image">
								<img src="{{asset('images/icons/return.png')}}" alt="">
							</div>
							<div class="box-title">
								<h3>Return 30 Days</h3>
							</div>
							<div class="clearfix"></div>
						</div><!-- /.box-header -->
					</div><!-- /.iconbox -->
				</div><!-- /.col-md-6 col-lg-3 -->
			</div><!-- /.row -->
		</div>
	</section>
</div>
@endsection