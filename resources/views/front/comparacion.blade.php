@php 
	$titulos=[];
	$precios=[];
	$carritos=[];
	$descripciones=[];
	$atributos_lista=[];
	$categoria2=[];
	foreach ($articulos as $key => $articulo) {
		$titulos[]=[
			'nombre' => $articulo->nombre,
			'slug' => $articulo->slug,
			'categoria_id' => $articulo->categoria_id,
			'foto' => $articulo->foto
		];
		$precios[]=$articulo->precio;
		$carritos[]=$articulo->slug;
		$descripciones[]=$articulo->descripcion;
		foreach ($articulo->atributos_articulo as $key => $lista) {
			$atributos_lista[$lista->nombre]=[
				'nombre' => $lista->nombre,
				'id' => $lista->id
			];
		}
		$categoria2[]=$articulo->categoria->nombre;

	}
@endphp

@extends('layouts.webpageshop.app')
@section('contenido')
	<section class="flat-breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumbs">
						<li class="trail-item">
							<a href="{{route('home')}}" title="">Inicio</a>
							<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
						</li>
						<li class="trail-item">
							<a href="{{route('TiendaFront')}}" title="">Tienda</a>
							<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
						</li>
						<li class="trail-end">
							<a href="{{route('TiendaComparacionFront')}}" title="">Comparar Articulos</a>
						</li>
					</ul><!-- /.breacrumbs -->
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.flat-breadcrumb -->	
	<style type="text/css">
		th{
			background-color: #eee;
			padding: 30px 30px 30px 20px;
			text-align: left;
			font-size: 16px;
			font-weight: bold;
			left: 0px;
			border: 1px solid #dedede;
		}
		td{
			text-align: center;
			padding: 20px;
			border: 1px solid #dedede;
		}
		.name{
			font-weight: bold;
			font-size: 16px;
			padding-top: 20px;
		}
		.btn-add-cart a {
		    display: inline-block;
		    background-color: #0153a6;
		    color: #fff;
		    font-size: 14px;
		    height: 45px;
		    line-height: 45px;
		    border-radius: 30px;
		    text-align: center;
		    position: relative;
		    overflow: hidden;
		    z-index: 3;
		    width: 90%;
		    margin-bottom: -15px!important;
		}
		.btn-add-cart a:before {
			content:'';
			position:absolute;
			width:100%;
			height:100%;
			top:0;
			left:0;
			z-index:-1;
			background:#f28b00;
			-webkit-transform:scaleX(0);
			transform:scaleX(0);
			-webkit-transform-origin:0 50%;
			transform-origin:0 50%;
			-webkit-transition-property:transform;
			transition-property:transform;
			-webkit-transition-duration:0.4s;
			transition-duration:0.4s;
			-webkit-transition-timing-function:ease-out;
			transition-timing-function:ease-out;
		}

		.btn-add-cart a:hover:before {
			-webkit-transform:scaleX(1);
			transform:scaleX(1);
			-webkit-transition-timing-function:cubic-bezier(0.52, 1.64, 0.37, 0.66);
			transition-timing-function:cubic-bezier(0.52, 1.64, 0.37, 0.66);
		}

		.btn-add-cart a img {
			padding-right:15px;
		}
	</style>

	<section class="flat-compare">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="wrap-compare">
						<div class="title">
							<h3>Comparación de Articulos

							<a href="{{route('TiendaEliminarTodaComparacionFront')}}" title="" class="pull-right">
								<img src="{{asset('images/icons/delete.png')}}" alt="">
							</a>
							</h3>
						</div>
						<div class="compare-content">
							@if(count($categoria2))
								<table class="" border="1px">	
									<tbody>
										<tr>
											<th>Productos</th>
											@foreach($titulos as $titulo)
												<td class="product">
													<div class="image">
														<a href="{{route('TiendaArticuloFront',$titulo['slug'])}}"><img src="{{$titulo['foto']}}" alt="" style="max-height:120px; "></a>
													</div>
													<div class="name">
														<a href="{{route('TiendaArticuloFront',$titulo['slug'])}}">{{$titulo['nombre']}}</a>
													</div>
												</td><!-- /.product -->
											@endforeach
										</tr>
										<tr>
											<th>Categoria</th>
											@foreach($categoria2 as $nombre)
												<td class="price">
													<a href="{{route('TiendaCategoriaFront',[$titulo['categoria_id'],$nombre])}}">
														{{$nombre}}
													</a>
												</td>
											@endforeach
											
										</tr>
										<tr>
											<th>Precio</th>
											@foreach($precios as $precio)
												<td class="price">
													${{$precio}}
												</td>
											@endforeach
											
										</tr>
										@foreach($atributos_lista as $lista)
											<tr>
												<th>{{$lista['nombre']}}</th>
												@foreach ($articulos as $articulo)
													@php 
														$resultado='Sin Información';
														foreach ($articulo->atributos_articulo as $value) {
															if ($value->nombre == $lista['nombre']) {
																$resultado=str_replace('|_|', ' ',$value->pivot->contenido);
															}
														}
													@endphp
													<td class="color">
														<p>
															{{$resultado}}
														</p>
													</td><!-- /.color -->
												@endforeach
											</tr>
										@endforeach
										<tr>
											<th></th>
											@foreach($carritos as $car)
											<td class="delete">
												<a href="{{route('TiendaCompararEliminarFront',$car)}}" title="">
													<img src="{{asset('images/icons/delete.png')}}" alt="">
												</a>
											</td><!-- /.delete -->
											@endforeach
										</tr>
									</tbody>
								</table><!-- /.table-compare -->
							@else
								<center><br>No hay articulos en Comparación<br><br></center>
							@endif
						</div><!-- /.compare-content -->
					</div><!-- /.wrap-compare -->
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.flat-compare -->
	@endsection