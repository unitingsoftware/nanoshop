@extends('layouts.webpageshop.app')
@section('contenido')
<style type="text/css">
	.slides img{
		max-height: 400px!important;
		max-width: 100%!important;
		width: auto!important;
	}
	.flex-control-thumbs {
	    margin: 30px 0 0!important;
	}
	.box-cart.style2 .btn-add-cart a {
	    width: 250px;
	}

	.style2  a:hover {
	    color: #fff!important;
	    background-color:#0153a6;
	}

	.style2  a {
	    color: #fff!important;
	    background-color:#484848;
	}

	.btn-add-cart  {
		display:inline-block;
		height:55px;
		line-height:55px;
		text-align:center;
		color:#fff;
		background-color:#0153a6;
		border-radius:30px;
		width:100%;
		font-size:16px;
		font-weight:600;
	}

	.btn-add-cart:hover {
		background-color:#2d2d2d;
		
	}

	.btn-add-cart img {
		padding-right:18px;
	}
</style>
<div class="boxed">
		<section class="flat-breadcrumb">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="breadcrumbs">
							<li class="trail-item">
								<a href="{{route('home')}} title="">Inicio</a>
								<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
							</li>
							<li class="trail-end">
								<a href="#" title="">Quienes Somos</a>
							</li>
						</ul><!-- /.breacrumbs -->
					</div><!-- /.col-md-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.flat-breadcrumb -->

		<section class="flat-product-detail">
			<div class="container">
				<div class="row">
					<!--<div class="col-md-12">
						<div class="f-title text-center">
							<h3 class="title text-uppercase">Nosotros</h3>
						</div>
					</div>-->
				</div>
				<br>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="about-page-cntent">
							<h3>Quienes Somos</h3>
							<p>Nanotec S.R.L. es una empresa argentina de tecnología dedicada a brindar soluciones de informática.Sus servicios abarcan desde la provisión de hardware, software, instalación y mantenimiento hasta los de consultoría y capacitación.</p>
							<blockquote>
								<p>Fundamentalmente orientada al sector público, tanto nacional como provincial, Nanotec S.R.L. cuenta con un staff de colaboradores idóneos y con amplia experiencia en el desarrollo de proyectos de IT de múltiples envergaduras.</p>
							</blockquote>
							<p>Su infraestructura operativa, así como también la calidad del servicio que otorga, la convierte en un socio estratégico confiable y sólido para sus clientes.</p>
						</div>
					</div>
					
				</div>
				<br>
				<br>
				<div class="row">
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div class="about-page-cntent">
							<h3>Computadoras personales</h3>
							<p>Realizamos configuraciones particulares para cada cliente de acuerdo a sus necesidades, variando desde PC’s estándar hasta configuraciones de altas prestaciones. Nos especializamos en provisiones de gran volumen.</p>
							
						</div>
										
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div class="about-page-cntent">
							<h3>Conectividad</h3>
							<p>Proveemos soluciones de infraestructura de red adecuadas a cada requerimiento en particular realizando diagnósticos de las necesidades del cliente a través de consultores especializados.</p>
							
						</div>
										
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div class="about-page-cntent">
							<h3>Servidores</h3>
							<p>Diseñamos infraestructura de hardware para servidores multipropósito definiendo la capacidad requerida en base a su utilización presente así como también proponemos estrategias de escalabilidad para crecimientos futuros.</p>
							
						</div>
										
					</div>
				</div>
				<br>
				<br>
				<div class="row">
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div class="about-page-cntent">
							<h3>Seguridad informática</h3>
							<p>Realizamos consultoría para la implementación y certificación de un Sistema de Gestión de Seguridad de la Información (SGSI) a través de ISO 27001.</p>
							
						</div>
										
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div class="about-page-cntent">
							<h3>Hardware informático</h3>
							<p>Trabajamos con las principales marcas de hardware ya sea para soluciones de impresión, networking, ofimática o licencias de software, lo cual nos otorga flexibilidad al momento de ofrecer los productos que mejor se adaptan en cada caso.</p>
							
						</div>
										
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div class="about-page-cntent">
							<h3>Servicio y garantía</h3>
							<p>Todos nuestros productos están garantizados por cada uno de los fabricantes, ofreciendo, además, servicios de garantía extendida. Para el caso de productos vendidos por nosotros o por terceros, donde la vigencia de la garantía ha expirado, brindamos servicios de mantenimiento preventivo y/o correctivo con provisión de repuestos.</p>
							
						</div>
										
					</div>
				</div>
			</div>
		</section><!-- /.flat-product-detail -->
	</div>
@endsection

@section('js')

@endsection