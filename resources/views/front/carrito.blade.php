@extends('layouts.webpageshop.app')
@section('contenido')
<style type="text/css">
	.table-cart tr td .price {
	    text-align: left;
	    line-height: 20px;
	    margin-right: 40px;
	    color: #989898;
	    font-size: 16px;
	    font-family: 'BloggerSans';
	}
</style>
<div class="boxed">
	<section class="flat-breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumbs">
						<li class="trail-item">
							<a href="{{route('home')}}" title="">Inicio</a>
							<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
						</li>
						<li class="trail-item">
							<a href="{{route('TiendaFront')}}" title="">Tienda</a>
							<span><img src="{{asset('images/icons/arrow-right.png')}}" alt=""></span>
						</li>
						<li class="trail-end">
							<a href="{{route('TiendaCarritoVerFront')}}" title="">Carrito</a>
						</li>
					</ul><!-- /.breacrumbs -->
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.flat-breadcrumb -->
	<section class="flat-shop-cart">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="flat-row-title style1">
							<h3>Carrito de Compras</h3>
						</div>
						<div class="table-cart">
							<table style="width: 90%;">
								<thead>
									<tr>
										<th>Articulo</th>
										<th>Cantidad</th>
										<th>Total</th>
										<th>Eliminar</th>
									</tr>
								</thead>
								<tbody>
									@php
										$total=0;
										$i=1;
									@endphp
									@foreach($articulos as $key => $articulo)
										@php
											$i++;
											if(auth()->user()){
												$precio=$articulo->precio*$articulo->pivot->cantidad;
												$cantidad=$articulo->pivot->cantidad;
											}else{
												$precio=$articulo->precio*$articulo->cantidad;
												$cantidad=$articulo->cantidad;
											}
											$total=$total+$precio;
										@endphp
										<tr>
											<td>
												<div class="img-product">
													<a href="{{route('TiendaArticuloFront',$articulo->slug)}}">
														<img src="{{$articulo->foto}}" alt="">
													</a>
												</div>
												<div class="name-product">
													<a href="{{route('TiendaArticuloFront',$articulo->slug)}}">{{$articulo->nombre}}</a>
													<br><div class="price" >
														${{$articulo->precio}} X 1
														<input type="hidden" id="precio{{$i}}" value="{{$articulo->precio}}">
													</div>
												</div>
											</td>
											<td>
												<div class="quanlity">
													<span class="btn-down" onclick="RestarCantidad({{$i}},{{$articulo->id}})"></span>
													<input type="number" id="cantidad{{$i}}" name="number" readonly value="{{$cantidad}}" min="1" max="100" placeholder="Quanlity">
													<span class="btn-up" onclick="SumarCantidad({{$i}},{{$articulo->id}})"></span>
												</div>
											</td>
											<td>
												<div class="total" id="total_articulo{{$articulo->id}}">
													${{$precio}}
												</div>
											</td>
											<td>
												<a href="{{route('TiendaCarritoEliminarFront',$articulo->slug)}}" title="">
													<img src="{{asset('images/icons/delete.png')}}" alt="">
												</a>
											</td>
										</tr>
									@endforeach
									@section('precio_carrito')
										{{$total}}
									@endsection
								</tbody>
							</table>
						</div><!-- /.table-cart -->
					</div><!-- /.col-lg-8 -->
					<div class="col-lg-4">
						<div class="cart-totals">
							<h3>Pedido</h3>
							<form action="#" method="get" accept-charset="utf-8">
								<table>
									<tbody>
										<tr>
											<td>Total</td>
											<td class="price-total">$<span id="total_final_pedido">{{$total}}</span></td>
										</tr>
									</tbody>
								</table>
								<div class="btn-cart-totals">
									<a href="{{route('TiendaPedidoGenerarFront')}}" class="checkout" title="">Realizar Pedido</a>
								</div><!-- /.btn-cart-totals -->
							</form><!-- /form -->
						</div><!-- /.cart-totals -->
					</div><!-- /.col-lg-4 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.flat-shop-cart -->
</div>
@endsection

@section('js')
	<script type="text/javascript">
		function SumarCantidad(lugar,id){
			let num = document.getElementById('cantidad'+lugar).value; 
			document.getElementById('cantidad'+lugar).value = parseInt(num)+1; 
			ActualizarCantidades(lugar,id);
		}

		function RestarCantidad(lugar,id){
			let num = document.getElementById('cantidad'+lugar).value; 
			if (num > 1) {
				document.getElementById('cantidad'+lugar).value = parseInt(num)-1; 
			}
			ActualizarCantidades(lugar,id);
		}
		function ActualizarCantidades(lugar,id){
		    var cantidad = $('#cantidad'+lugar).val();
		    var precio = $('#precio'+lugar).val();
		    $.ajax({
		        type:'GET',
		        url:'{{route("TiendaCarritoActualizarAjaxFront")}}',
		        dataType: "json",
		        data:{id:id,cantidad:cantidad},
		        success:function(data){
		        	document.getElementById('cantidad_carrito_header_'+id).innerHTML = cantidad; 
		        	document.getElementById('total_articulo'+id).innerHTML = parseInt(cantidad*precio); 
		        	document.getElementById('total_final_pedido').innerHTML = data; 
		        	document.getElementById('total_final_pedido_header').innerHTML = data; 
		        	document.getElementById('total_fuera_carrito').innerHTML = data; 
		        }
		    });
		  }
	</script>
@endsection