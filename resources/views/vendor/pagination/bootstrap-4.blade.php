@if ($paginator->hasPages())
    <span>
        {{$paginator->total()}} Resultados
    </span>
    <ul class="flat-pagination style1" role="navigation">

        @if ($paginator->onFirstPage())
            <li class="active">
                <a>&lsaquo;</a>
            </li>
        @else
            <li>
                <a href="{{ $paginator->previousPageUrl() }}">&lsaquo;</a>
            </li>
        @endif


        @foreach ($elements as $element)

            @if (is_string($element))
                <li class="active"><span>{{ $element }}</span></li>
            @endif


            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active"><a>{{ $page }}</a></li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach


        @if ($paginator->hasMorePages())
            <li>
                <a href="{{ $paginator->nextPageUrl() }}">&rsaquo;</a>
            </li>
        @else
            <li class="active">
                <a>&rsaquo;</a>
            </li>
        @endif
    </ul>
@endif
