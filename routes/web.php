<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();

Route::group(['middleware' => ['web']], function () {
	Route::get('/', 'FrontController@index')->name('home');
	Route::get('/quienes-somos', 'FrontController@QuienesSomosFront')->name('QuienesSomosFront');
	Route::get('/garantia-y-soporte', 'FrontController@GarantiaSoporteFront')->name('GarantiaSoporteFront');
	Route::get('/contacto', 'FrontController@ContactoFront')->name('ContactoFront');
	Route::get('/cliente', 'FrontController@ClientesFront')->name('ClientesFront');
	

	Route::post('/garantia-y-soporte', 'FrontController@ContactoCorreoFront')->name('ContactoCorreoFront');
	Route::post('/contacto', 'FrontController@GarantiaSoporteCorreoFront')->name('GarantiaSoporteCorreoFront');

	Route::group(['prefix' => 'tienda','middleware' => ['web']], function () {
		Route::bind('articulo', function ($value) {
		    return App\Articulo::where('slug', $value)->limit(1)->first()?? abort(404);
		});

		Route::get('/', 'FrontController@tienda')->name('TiendaFront');
		Route::get('/buscar/', 'FrontController@BuscadorArticulo')->name('TiendaBuscadorArticuloFront');
		Route::get('/categoria/{id}/{nombre}', 'FrontController@categoria')->name('TiendaCategoriaFront');
		Route::get('/articulo/{articulo}/', 'FrontController@articulo')->name('TiendaArticuloFront');
		Route::post('/articulo/presupuesto/{articulo}/', 'FrontController@PresupuestoArticulo')->name('TiendaPresupuestoArticuloFront');
		
		Route::get('/carrito/', 'FrontController@VerCarrito')->name('TiendaCarritoVerFront');
		Route::get('/carrito/agregar/{articulo}', 'FrontController@AgregarCarrito')->name('TiendaCarritoAgregarFront');
		Route::get('/carrito/eliminar/{articulo}', 'FrontController@EliminarCarrito')->name('TiendaCarritoEliminarFront');
		Route::get('/carrito/vaciar/', 'FrontController@VaciarCarrito')->name('TiendaCarritoVaciarFront');
		Route::get('/carrito/actualizar/', 'FrontController@ActualizarCarrito')->name('TiendaCarritoActualizarAjaxFront');
	
		Route::get('articulos/comparar/', 'FrontController@VerComparacion')->name('TiendaComparacionFront')->middleware('web');
		Route::get('articulos/comparar/agregar/{articulo}', 'FrontController@AgregarArticuloComparacion')->name('TiendaCompararAgregarFront')->middleware('web');
		Route::get('articulo/comparar/eliminar/{articulo}', 'FrontController@EliminarComparacion')->name('TiendaCompararEliminarFront');
		Route::get('articulo/comparar/vaciar', 'FrontController@EliminarTodaComparacion')->name('TiendaEliminarTodaComparacionFront');

	});

	Route::get('usuarios/login/', 'FrontController@login')->name('TiendaUsuariosLoginFront')->middleware('web');
	//Solo usuario Logeados	
	Route::group(['prefix' => 'usuarios','middleware' => ['web','AdminMember']], function () {
		Route::get('/MiCuenta/', 'FrontController@MiCuenta')->name('TiendaMiCuentaFront');
		Route::get('/pedidos/', 'FrontController@ListarPedido')->name('TiendaPedidoListarFront');
		Route::get('/pedidos/ver/{id}', 'FrontController@VerPedido')->name('TiendaPedidoVerFront');
		Route::get('/pedidos/generarPedido/', 'FrontController@GenerarPedido')->name('TiendaPedidoGenerarFront');
		Route::get('/pedidos/EjecutarPedido/', 'FrontController@EjecutarPedido')->name('TiendaPedidoEjecutarFront');
	});
	

		

});

//Admin
Route::group(['prefix' => 'indexAdmin', 'middleware' => ['auth','AdminBK']], function () {
	Route::get('/', 'HomeController@indexAdmin')->name('adminPanel');
	
	//Categorias
	Route::group(['prefix' => 'categorias'], function () {
		Route::group(['prefix' => 'atributos'], function () {
			Route::get('/{id}', 'AtributoController@index')->name('ListarAtributoCategoriaAdmin');
			Route::get('crear/{id}', 'AtributoController@crear')->name('CrearAtributoCategoriaAdmin');
			Route::post('add/{id}', 'AtributoController@add')->name('GuardarAtributoCategoriaAdmin');
			Route::get('editar/{categoria}/{atributo}', 'AtributoController@editar')->name('EditarAtributoCategoriaAdmin');
			Route::post('update/{categoria}/{atributo}', 'AtributoController@update')->name('UpdateAtributoCategoriaAdmin');
			Route::get('retirar/{categoria}/{atributo}', 'AtributoController@retirar')->name('RetirarAtributoCategoriaAdmin');
			Route::get('requerir/{categoria}/{atributo}', 'AtributoController@requerir')->name('RequerirAtributoCategoriaAdmin');
			Route::get('eliminar/{categoria}/{atributo}', 'AtributoController@eliminar')->name('EliminarAtributoCategoriaAdmin');
			Route::get('articulo/ajax/{id?}', 'AtributoController@ajaxArticulo')->name('ArticuloAtributoCategoriaAdmin');
			//Atributos Categoria Ajax Crear Producto
		});

		Route::get('/{id?}', 'CategoriaController@index')->name('ListarCategoriasAdmin');
		Route::get('crear/{id?}', 'CategoriaController@crear')->name('CrearCategoriasAdmin');
		Route::post('add', 'CategoriaController@add')->name('GuardarCategoriaAdmin');
		Route::get('editar/{id}', 'CategoriaController@editar')->name('EditarCategoriasAdmin');
		Route::post('update/{id}', 'CategoriaController@update')->name('UpdateCategoriasAdmin');
		Route::get('eliminar/{id}', 'CategoriaController@eliminar')->name('EliminarCategoriasAdmin');
	});

	Route::group(['prefix' => 'articulos'], function () {
		Route::get('/', 'ArticuloController@index')->name('ListarArticulosAdmin');
		Route::get('crear/', 'ArticuloController@crear')->name('CrearArticuloAdmin');
		Route::post('add/', 'ArticuloController@add')->name('GuardarArticuloAdmin');
		Route::get('editar/{id}', 'ArticuloController@editar')->name('EditarArticulosAdmin');
		Route::post('update/{id}', 'ArticuloController@update')->name('UpdateArticulosAdmin');
		Route::get('eliminar/{id}', 'ArticuloController@eliminar')->name('EliminarArticulosAdmin');
		
	});

	Route::group(['prefix' => 'pedidos'], function () {
		Route::get('/{estado?}', 'PedidoController@index')->name('ListarPedidosAdmin');
		Route::get('/ver/{id}', 'PedidoController@VerPedido')->name('VerPedidoAdmin');
		Route::post('/ver/{id}', 'PedidoController@CambioEstadoPedido')->name('CambioEstadoPedidoAdmin');
	});

	Route::group(['prefix' => 'presupuestos'], function () {
		Route::get('/{estado?}', 'PresupuestoController@index')->name('ListarPresupuestosAdmin');
		Route::get('/ver/{id}', 'PresupuestoController@ver')->name('VerPresupuestoAdmin');
		Route::post('/ver/{id}', 'PresupuestoController@CambioEstado')->name('CambioEstadoPresupuestoAdmin');
	});

	Route::group(['prefix' => 'usuarios'], function () {
		Route::get('/', 'UsuarioController@index')->name('ListarUsuariosAdmin');
		Route::get('crear/', 'UsuarioController@crear')->name('CrearUsuariosAdmin');
		Route::post('crear/', 'UsuarioController@add')->name('AddUsuariosAdmin');
		Route::get('/ver/{id}', 'UsuarioController@ver')->name('VerUsuariosAdmin');
		Route::post('/ver/{id}', 'UsuarioController@update')->name('UpdateUsuariosAdmin');
		Route::get('eliminar/{id}', 'UsuarioController@eliminar')->name('EliminarUsuariosAdmin');
	});

});