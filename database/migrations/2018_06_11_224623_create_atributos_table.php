<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtributosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('atributos', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('tipo',['select','text','number','number_list']);
            $table->string('nombre');
            $table->string('contenido')->nullable()->default('');
            $table->integer('categoria_id')->unsigned();
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade')->onUpdate('cascade');
        });
        
        //Tablas Pivot
        Schema::create('atributo_articulo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('atributo_id')->unsigned();
            $table->integer('articulo_id')->unsigned();
            $table->string('contenido');
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('atributo_id')->references('id')->on('atributos')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('articulo_id')->references('id')->on('articulos')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('atributo_categoria', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categoria_id')->unsigned();
            $table->integer('atributo_id')->unsigned();
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('atributo_id')->references('id')->on('atributos')->onDelete('cascade')->onUpdate('cascade');
        });
 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atributos');
        Schema::dropIfExists('atributo_articulo');
        Schema::dropIfExists('atributo_categoria');
    }
}
