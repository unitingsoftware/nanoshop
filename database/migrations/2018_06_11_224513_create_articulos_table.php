<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     
        Schema::create('articulos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique()->nullable();
            $table->string('nombre');
            $table->decimal('precio', 20, 2);
            $table->string('foto')->nullable();
            $table->text('galeria')->nullable();
            $table->text('descripcion')->nullable();
            $table->integer('categoria_id')->unsigned();
            $table->boolean('presupuesto')->default(false);
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade')->onUpdate('cascade');
        });

        //Tablas Pivot
        Schema::create('articulos_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('articulo_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->enum('tipo',['carrito','comparar']);
            $table->integer('cantidad')->unsigned();
            $table->timestamps();

            $table->foreign('articulo_id')->references('id')->on('articulos')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos');
        Schema::dropIfExists('articulos_usuarios');
    }
}
