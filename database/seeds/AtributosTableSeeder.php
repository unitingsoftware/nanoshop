<?php

use Illuminate\Database\Seeder;

class AtributosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**************************
		* Atributos
		***************************/
		DB::table('atributos')->insert([				
			"id" => 1,
			"categoria_id" => 1,
			"tipo" => "select",
			"nombre" => "Modelo",
			"contenido" => "HP,Lenevo,Dell"	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 2,
			"categoria_id" => 1,
			"tipo" => "text",
			"nombre" => "Garantia",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 3,
			"categoria_id" => 1,
			"tipo" => "number",
			"nombre" => "Numero de Piezas",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 4,
			"categoria_id" => 1,
			"tipo" => "number_list",
			"nombre" => "Cantidad de Memoria",
			"contenido" => "MB,GB,TB"	
		]);				
						
						
		DB::table('atributos')->insert([				
			"id" => 5,
			"categoria_id" => 23,
			"tipo" => "select",
			"nombre" => "Modelo",
			"contenido" => "HP,Lenevo,Dell"	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 6,
			"categoria_id" => 23,
			"tipo" => "text",
			"nombre" => "Garantia",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 7,
			"categoria_id" => 23,
			"tipo" => "number",
			"nombre" => "Numero de Piezas",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 8,
			"categoria_id" => 23,
			"tipo" => "number_list",
			"nombre" => "Cantidad de Memoria",
			"contenido" => "MB,GB,TB"	
		]);				
						
						
		DB::table('atributos')->insert([				
			"id" => 9,
			"categoria_id" => 44,
			"tipo" => "select",
			"nombre" => "Modelo",
			"contenido" => "HP,Lenevo,Dell"	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 10,
			"categoria_id" => 44,
			"tipo" => "text",
			"nombre" => "Garantia",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 11,
			"categoria_id" => 44,
			"tipo" => "number",
			"nombre" => "Numero de Piezas",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 12,
			"categoria_id" => 44,
			"tipo" => "number_list",
			"nombre" => "Cantidad de Memoria",
			"contenido" => "MB,GB,TB"	
		]);				
						
						
		DB::table('atributos')->insert([				
			"id" => 13,
			"categoria_id" => 65,
			"tipo" => "select",
			"nombre" => "Modelo",
			"contenido" => "HP,Lenevo,Dell"	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 14,
			"categoria_id" => 65,
			"tipo" => "text",
			"nombre" => "Garantia",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 15,
			"categoria_id" => 65,
			"tipo" => "number",
			"nombre" => "Numero de Piezas",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 16,
			"categoria_id" => 65,
			"tipo" => "number_list",
			"nombre" => "Cantidad de Memoria",
			"contenido" => "MB,GB,TB"	
		]);				
						
						
		DB::table('atributos')->insert([				
			"id" => 17,
			"categoria_id" => 65,
			"tipo" => "select",
			"nombre" => "Modelo",
			"contenido" => "HP,Lenevo,Dell"	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 18,
			"categoria_id" => 65,
			"tipo" => "text",
			"nombre" => "Garantia",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 19,
			"categoria_id" => 65,
			"tipo" => "number",
			"nombre" => "Numero de Piezas",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 20,
			"categoria_id" => 65,
			"tipo" => "number_list",
			"nombre" => "Cantidad de Memoria",
			"contenido" => "MB,GB,TB"	
		]);				
						
						
		DB::table('atributos')->insert([				
			"id" => 21,
			"categoria_id" => 66,
			"tipo" => "select",
			"nombre" => "Modelo",
			"contenido" => "HP,Lenevo,Dell"	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 22,
			"categoria_id" => 66,
			"tipo" => "text",
			"nombre" => "Garantia",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 23,
			"categoria_id" => 66,
			"tipo" => "number",
			"nombre" => "Numero de Piezas",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 24,
			"categoria_id" => 66,
			"tipo" => "number_list",
			"nombre" => "Cantidad de Memoria",
			"contenido" => "MB,GB,TB"	
		]);				
						
						
		DB::table('atributos')->insert([				
			"id" => 25,
			"categoria_id" => 67,
			"tipo" => "select",
			"nombre" => "Modelo",
			"contenido" => "HP,Lenevo,Dell"	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 26,
			"categoria_id" => 67,
			"tipo" => "text",
			"nombre" => "Garantia",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 27,
			"categoria_id" => 67,
			"tipo" => "number",
			"nombre" => "Numero de Piezas",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 28,
			"categoria_id" => 67,
			"tipo" => "number_list",
			"nombre" => "Cantidad de Memoria",
			"contenido" => "MB,GB,TB"	
		]);				
						
						
		DB::table('atributos')->insert([				
			"id" => 29,
			"categoria_id" => 68,
			"tipo" => "select",
			"nombre" => "Modelo",
			"contenido" => "HP,Lenevo,Dell"	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 30,
			"categoria_id" => 68,
			"tipo" => "text",
			"nombre" => "Garantia",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 31,
			"categoria_id" => 68,
			"tipo" => "number",
			"nombre" => "Numero de Piezas",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 32,
			"categoria_id" => 68,
			"tipo" => "number_list",
			"nombre" => "Cantidad de Memoria",
			"contenido" => "MB,GB,TB"	
		]);				
						
						
		DB::table('atributos')->insert([				
			"id" => 33,
			"categoria_id" => 69,
			"tipo" => "select",
			"nombre" => "Modelo",
			"contenido" => "HP,Lenevo,Dell"	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 34,
			"categoria_id" => 69,
			"tipo" => "text",
			"nombre" => "Garantia",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 35,
			"categoria_id" => 69,
			"tipo" => "number",
			"nombre" => "Numero de Piezas",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 36,
			"categoria_id" => 69,
			"tipo" => "number_list",
			"nombre" => "Cantidad de Memoria",
			"contenido" => "MB,GB,TB"	
		]);				
						
						
		DB::table('atributos')->insert([				
			"id" => 37,
			"categoria_id" => 70,
			"tipo" => "select",
			"nombre" => "Modelo",
			"contenido" => "HP,Lenevo,Dell"	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 38,
			"categoria_id" => 70,
			"tipo" => "text",
			"nombre" => "Garantia",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 39,
			"categoria_id" => 70,
			"tipo" => "number",
			"nombre" => "Numero de Piezas",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 40,
			"categoria_id" => 70,
			"tipo" => "number_list",
			"nombre" => "Cantidad de Memoria",
			"contenido" => "MB,GB,TB"	
		]);				
						
						
		DB::table('atributos')->insert([				
			"id" => 41,
			"categoria_id" => 71,
			"tipo" => "select",
			"nombre" => "Modelo",
			"contenido" => "HP,Lenevo,Dell"	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 42,
			"categoria_id" => 71,
			"tipo" => "text",
			"nombre" => "Garantia",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 43,
			"categoria_id" => 71,
			"tipo" => "number",
			"nombre" => "Numero de Piezas",
			"contenido" => ""	
		]);				
						
		DB::table('atributos')->insert([				
			"id" => 44,
			"categoria_id" => 71,
			"tipo" => "number_list",
			"nombre" => "Cantidad de Memoria",
			"contenido" => "MB,GB,TB"	
		]);		
    }
}
