    <?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'id' => 1,
            'name' => str_random(10),
            'user' => 'admin',
            'telefono' => '123456',
            'email' => 'admin@admin.com',
            'type' => 'admin',
            'password' => bcrypt('admin'),
        ]);

        DB::table('users')->insert([
            'id' => 2,
            'name' => 'Alejandro Fuentes',
            'user' => 'Alejandro',
            'telefono' => '123456',
            'email' => 'alejandro@example.com',
            'type' => 'member',
            'password' => bcrypt('123456'),
        ]);

        DB::table('users')->insert([
            'id' => 3,
            'name' => 'Ruben Nazoa',
            'user' => 'Ruben',
            'telefono' => '123456',
            'email' => 'ruben@example.com',
            'type' => 'member',
            'password' => bcrypt('123456'),
        ]);

        DB::table('users')->insert([
            'id' => 4,
            'name' => 'Fabrizio Azuaje',
            'user' => 'Fabrizio',
            'telefono' => '123456',
            'email' => 'fabrizio@example.com',
            'type' => 'member',
            'password' => bcrypt('123456'),
        ]);
    }
}
