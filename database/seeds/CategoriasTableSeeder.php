<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	/**************************
		* Computadoras & Laptops
		***************************/
		
        DB::table('categorias')->insert([
            'id' => 1,
            'padre_id' => 0,
            'nombre' => 'Computadoras & Laptops'
        ]);

	        DB::table('categorias')->insert([
	            'id' => 2,
	            'padre_id' => 1,
	            'nombre' => 'Computadoras & Laptops'
	        ]);

		        DB::table('categorias')->insert([
		            'id' => 3,
		            'padre_id' => 2,
		            'nombre' => 'Pcs de Escritorio'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 4,
		            'padre_id' => 2,
		            'nombre' => 'Laptops'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 5,
		            'padre_id' => 2,
		            'nombre' => 'Servidores'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 6,
		            'padre_id' => 2,
		            'nombre' => 'Pc Gamers'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 7,
		            'padre_id' => 2,
		            'nombre' => 'Accesorios'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 8,
		            'padre_id' => 2,
		            'nombre' => 'Repuestos'
		        ]);

	        
	        DB::table('categorias')->insert([
	            'id' => 9,
	            'padre_id' => 1,
	            'nombre' => 'Audio & Video'
	        ]);

		        DB::table('categorias')->insert([
		            'id' => 10,
		            'padre_id' => 9,
		            'nombre' => 'Cornetas'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 11,
		            'padre_id' => 9,
		            'nombre' => 'Monitores'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 12,
		            'padre_id' => 9,
		            'nombre' => 'Auriculares'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 13,
		            'padre_id' => 9,
		            'nombre' => 'Microfonos'
		        ]);

	        
	        DB::table('categorias')->insert([
	            'id' => 14,
	            'padre_id' => 1,
	            'nombre' => 'Perifericos'
	        ]);

		        DB::table('categorias')->insert([
		            'id' => 15,
		            'padre_id' => 14,
		            'nombre' => 'Mauses'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 16,
		            'padre_id' => 14,
		            'nombre' => 'Teclados'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 17,
		            'padre_id' => 14,
		            'nombre' => 'Teclados'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 18,
		            'padre_id' => 14,
		            'nombre' => 'TouchPad'
		        ]);


	        DB::table('categorias')->insert([
	            'id' => 19,
	            'padre_id' => 1,
	            'nombre' => 'Perifericos'
	        ]);

		        DB::table('categorias')->insert([
		            'id' => 20,
		            'padre_id' => 19,
		            'nombre' => 'Mesas'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 21,
		            'padre_id' => 19,
		            'nombre' => 'Sillas'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 22,
		            'padre_id' => 19,
		            'nombre' => 'Luces'
		        ]);
    	

    	/**************************
		* Telefonos & Tablets
		***************************/

        DB::table('categorias')->insert([
            'id' => 23,
            'padre_id' => 0,
            'nombre' => 'Telefonos & Tablets'
        ]);

	        DB::table('categorias')->insert([
	            'id' => 24,
	            'padre_id' => 23,
	            'nombre' => 'Telefonos & Tablets'
	        ]);

		        DB::table('categorias')->insert([
		            'id' => 25,
		            'padre_id' => 24,
		            'nombre' => 'Gama Baja'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 26,
		            'padre_id' => 24,
		            'nombre' => 'Gama Media'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 27,
		            'padre_id' => 24,
		            'nombre' => 'Gama Alta'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 28,
		            'padre_id' => 24,
		            'nombre' => 'Satelitales'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 29,
		            'padre_id' => 24,
		            'nombre' => 'Ecologicos'
		        ]);

	        
	        DB::table('categorias')->insert([
	            'id' => 30,
	            'padre_id' => 23,
	            'nombre' => 'Audio & Video'
	        ]);

		        DB::table('categorias')->insert([
		            'id' => 31,
		            'padre_id' => 30,
		            'nombre' => 'Cornetas'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 32,
		            'padre_id' => 30,
		            'nombre' => 'Monitores'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 33,
		            'padre_id' => 30,
		            'nombre' => 'Auriculares'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 34,
		            'padre_id' => 30,
		            'nombre' => 'Microfonos'
		        ]);

	        
	        DB::table('categorias')->insert([
	            'id' => 35,
	            'padre_id' => 23,
	            'nombre' => 'Perifericos'
	        ]);

		        DB::table('categorias')->insert([
		            'id' => 36,
		            'padre_id' => 35,
		            'nombre' => 'Mauses'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 37,
		            'padre_id' => 35,
		            'nombre' => 'Teclados'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 38,
		            'padre_id' => 35,
		            'nombre' => 'Teclados'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 39,
		            'padre_id' => 35,
		            'nombre' => 'TouchPad'
		        ]);


	        DB::table('categorias')->insert([
	            'id' => 40,
	            'padre_id' => 23,
	            'nombre' => 'Perifericos'
	        ]);

		        DB::table('categorias')->insert([
		            'id' => 41,
		            'padre_id' => 40,
		            'nombre' => 'Mesas'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 42,
		            'padre_id' => 40,
		            'nombre' => 'Sillas'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 43,
		            'padre_id' => 10,
		            'nombre' => 'Luces'
		        ]);

    	/**************************
		* Hogar
		***************************/

        DB::table('categorias')->insert([
            'id' => 44,
            'padre_id' => 0,
            'nombre' => 'Hogar'
        ]);

	        DB::table('categorias')->insert([
	            'id' => 45,
	            'padre_id' => 44,
	            'nombre' => 'Hogar'
	        ]);

		        DB::table('categorias')->insert([
		            'id' => 46,
		            'padre_id' => 45,
		            'nombre' => 'Gama Baja'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 47,
		            'padre_id' => 45,
		            'nombre' => 'Gama Media'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 48,
		            'padre_id' => 45,
		            'nombre' => 'Gama Alta'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 49,
		            'padre_id' => 45,
		            'nombre' => 'Satelitales'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 50,
		            'padre_id' => 45,
		            'nombre' => 'Ecologicos'
		        ]);

	        
	        DB::table('categorias')->insert([
	            'id' => 51,
	            'padre_id' => 44,
	            'nombre' => 'Audio & Video'
	        ]);

		        DB::table('categorias')->insert([
		            'id' => 52,
		            'padre_id' => 51,
		            'nombre' => 'Cornetas'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 53,
		            'padre_id' => 51,
		            'nombre' => 'Monitores'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 54,
		            'padre_id' => 51,
		            'nombre' => 'Auriculares'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 55,
		            'padre_id' => 51,
		            'nombre' => 'Microfonos'
		        ]);

	        
	        DB::table('categorias')->insert([
	            'id' => 56,
	            'padre_id' => 44,
	            'nombre' => 'Perifericos'
	        ]);

		        DB::table('categorias')->insert([
		            'id' => 57,
		            'padre_id' => 56,
		            'nombre' => 'Mauses'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 58,
		            'padre_id' => 56,
		            'nombre' => 'Teclados'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 59,
		            'padre_id' => 56,
		            'nombre' => 'Teclados'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 60,
		            'padre_id' => 56,
		            'nombre' => 'TouchPad'
		        ]);


	        DB::table('categorias')->insert([
	            'id' => 61,
	            'padre_id' => 44,
	            'nombre' => 'Perifericos'
	        ]);

		        DB::table('categorias')->insert([
		            'id' => 62,
		            'padre_id' => 57,
		            'nombre' => 'Mesas'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 63,
		            'padre_id' => 57,
		            'nombre' => 'Sillas'
		        ]);

		        DB::table('categorias')->insert([
		            'id' => 64,
		            'padre_id' => 57,
		            'nombre' => 'Luces'
		        ]);

		        
		/**************************
		* Software
		***************************/

		DB::table('categorias')->insert([
            'id' => 65,
            'padre_id' => 0,
            'nombre' => 'Software'
        ]);


		/**************************
		* TV & Audio
		***************************/

		DB::table('categorias')->insert([
            'id' => 66,
            'padre_id' => 0,
            'nombre' => 'TV & Audio'
        ]);


		/**************************
		* Deportes
		***************************/
		DB::table('categorias')->insert([
            'id' => 67,
            'padre_id' => 0,
            'nombre' => 'Deportes'
        ]);


		/**************************
		* Juegos & Juguetes
		***************************/
		DB::table('categorias')->insert([
            'id' => 68,
            'padre_id' => 0,
            'nombre' => 'Juegos & Juguetes'
        ]);


		/**************************
		* Video Camaras
		***************************/
		DB::table('categorias')->insert([
            'id' => 69,
            'padre_id' => 0,
            'nombre' => 'Video Camaras'
        ]);


		/**************************
		* Accesorios
		***************************/
		DB::table('categorias')->insert([
            'id' => 70,
            'padre_id' => 0,
            'nombre' => 'Accesorios'
        ]);


		/**************************
		* Seguridad
		***************************/
		DB::table('categorias')->insert([
            'id' => 71,
            'padre_id' => 0,
            'nombre' => 'Seguridad'
        ]);
    }
}
