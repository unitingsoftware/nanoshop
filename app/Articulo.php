<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    protected $table = 'articulos';

    public function categoria(){
    	return $this->belongsTo('App\Categoria');
	}

	public function atributos_articulo(){
    	return $this->belongsToMany('App\Atributo','atributo_articulo')->withTimestamps()->withPivot('contenido');
	}

	public function pedido(){
		return $this->belongsTo('App\Pedido');
	}

	public function articulos_pedido(){
    	return $this->belongsToMany('App\Pedido','articulos_pedido')->withTimestamps()->withPivot('precio','cantidad');
	}

	public function articulos_usuarios(){
    	return $this->belongsToMany('App\User','articulos_usuarios')->withTimestamps()->withPivot('tipo','cantidad');
	}
}
