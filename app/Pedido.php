<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
	protected $table = 'pedidos';

    public function articulos(){
    	return $this->hasMany('App\Articulo');
    }

    public function articulos_pedido(){
    	return $this->belongsToMany('App\Articulo','articulos_pedido')->withTimestamps()->withPivot('precio','cantidad');
	}

    public function user(){
    	return $this->belongsTo('App\User');
    }

}
