<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categorias';

    public function atributos_categorias(){
    	return $this->belongsToMany('App\Atributo')->withTimestamps()->withPivot('id');
	}

    public function atributos(){
    	return $this->hasMany('App\Atributo');
	}
}
