<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class AdminMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()) {
            return $next($request);
        }else{
            Session::put('redirect_to',url()->current());
            return redirect(route('TiendaUsuariosLoginFront'));
        }
    }
}
