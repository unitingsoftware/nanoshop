<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use DB;

class CategoriaController extends Controller
{
    public function index($id = 0){
    	$categorias=DB::table('categorias')->where('padre_id', $id)->where('active','1')->get();
    	if ($id == 0) {
    		$padre=new \stdClass();
    		$padre->id=0;
    		$padre->nombre='';
    	}else{
    		$padre=Categoria::find($id);
    		$categorias=DB::table('categorias')->where('padre_id', $id)->where('active','1')->get();
    	}
    	return view('adminlte.categorias.index',[
    		'categorias'=>$categorias,
    		'padre'=>$padre
    	]);
    }

    public function crear($id = 0){
    	if ($id == 0) {
    		$padre=new \stdClass();
    		$padre->id=0;
    		$padre->nombre='';
    		$categorias=null;
    	}else{
    		$padre=Categoria::find($id);
    		$categorias=DB::table('categorias')->where('id', $id)->get();
    	}

    	return view('adminlte.categorias.crear',[
    		'padre_id'=>$padre->id,
    		'padre'=>$padre->nombre,
    		'categorias'=>$categorias
    	]);
    }

    public function add(Request $request){
    	$categoria=new Categoria();
    	$categoria->nombre=$request->nombre;
    	$categoria->padre_id=$request->padre_id;
    	if($categoria->save()) {
	    	return redirect(route('ListarCategoriasAdmin').'/'.$request->padre_id)->with([
				'mensage' => 'Categoria guardada.',
				'type' => 'success',
				'title' => 'Categoria'
			]);
		}else{
			return redirect()->back()->with([
				'mensage' => 'Error al guardar la categoria.',
				'type' => 'error',
				'title' => 'Categoria'
			]);	
		}
    }

    public function editar($id){
    	$categoria=Categoria::find($id);
    	$padre=Categoria::find($categoria->padre_id);
		
		if (!$padre) {
    		$padre=new \stdClass();
    		$padre->id=0;
    		$padre->nombre='';
		}
		return view('adminlte.categorias.editar',[
    		'padre_id'=>$padre->id,
    		'padre'=>$padre->nombre,
    		'categoria'=>$categoria
    	]);

    }

    public function update($id,Request $request){
    	$categoria=Categoria::find($id);
    	$categoria->nombre=$request->nombre;
		
		if($categoria->save()) {
	    	return redirect(route('ListarCategoriasAdmin').'/'.$request->padre_id)->with([
				'mensage' => 'Categoria editada.',
				'type' => 'success',
				'title' => 'Categoria'
			]);
		}else{
			return redirect()->back()->with([
				'mensage' => 'Error al editar la categoria.',
				'type' => 'error',
				'title' => 'Categoria'
			]);	
		}

    }

    public function eliminar($id){
        $categoria=Categoria::find($id);
        $categoria->active=0;
        $categoria->save();
        
        $arbol=$this->ArbolDeCategorias($id);

        for ($i=0; $i < count($arbol); $i++) {
            $eliminar=Categoria::find($arbol[$i]['id']);
            $eliminar->active=0;
            $eliminar->save();
        }
        
        return redirect(route('ListarCategoriasAdmin').'/'.$categoria->padre_id)->with([
            'mensage' => 'Categoria Eliminada.',
            'type' => 'success',
            'title' => 'Eliminar'
        ]);
        
    }

    public function ArbolDeCategorias($padre_id = 0, $arbol_de_categorias = ''){
        if (!is_array($arbol_de_categorias)){   
            $arbol_de_categorias = array();
        }

        $categoria=Categoria::where('padre_id',$padre_id)->get();

        for ($i=0; $i < count($categoria); $i++) { 
            $arbol_de_categorias[] = array("id" => $categoria[$i]->id, "nombre" => $categoria[$i]->nombre, "padre_id" => $categoria[$i]->padre_id);
            $arbol_de_categorias = $this->ArbolDeCategorias($categoria[$i]->id, $arbol_de_categorias);
        }

        return $arbol_de_categorias;
    }
}
