<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Articulo;
use App\Categoria;
use App\Atributo;
use App\Pedido;
use App\User;
use App\Presupuesto;
use Illuminate\Support\Str as Str;
use DB;

class PresupuestoController extends Controller{

    public function index($estado = null){
    	if ($estado == null) {
    		$presupuestos=Presupuesto::get();
    	}else{
    		$presupuestos=Presupuesto::where('estado',$estado)->get();
    	}
    	$presupuestos->each(function($presupuestos){
    		$presupuestos->user;
    		$presupuestos->articulo;
    	});
    	return view('adminlte.presupuestos.index',[
    		'presupuestos' => $presupuestos
    	]);
    }

    public function ver($id){        
    	$presupuesto=Presupuesto::where('id',$id)->get();
    	$presupuesto->each(function($presupuesto){
            $presupuesto->user;
    		$presupuesto->articulo;
        });
    	return view('adminlte.presupuestos.ver',[
    		'presupuesto' => $presupuesto[0]
    	]);
    }

    public function CambioEstado($id, Request $request){
    	$presupuesto=Presupuesto::find($id);
    	$presupuesto->estado=$request->estado;
    	$presupuesto->save();
    	return redirect()->back()->with([
			'mensage' => 'Estado Actualizado.',
			'type' => 'success',
			'title' => 'Presupuestos'
		]);
    }
}
