<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Articulo;
use App\Categoria;
use App\Atributo;
use Illuminate\Support\Str as Str;
use DB;

class ArticuloController extends Controller
{
    public function index(){
        $articulos=Articulo::all();
        $articulos->each(function($articulos){
            $articulos->categoria;
        });

    	return view('adminlte.articulos.index',[
    		'articulos'=> $articulos
    	]);
    }

    public function crear(){
    	$categorias=$this->ArbolDeCategoriasSelect();
    	return view('adminlte.articulos.crear',[
    		'categoria' => $categorias
    	]);
    }

    public function add(Request $request){

    	$articulo=new Articulo();
    	$articulo->nombre=$request->nombre;
    	$articulo->precio=$request->precio;
    	$articulo->foto=$request->foto;
    	$articulo->galeria=$request->galeria;
    	$articulo->descripcion=$request->descripcion;
        $articulo->categoria_id=$request->categoria_id;
    	$articulo->presupuesto=$request->presupuesto;
    	$articulo->save();
    	$articulo->slug='NANO'.$articulo->id.'-'.Str::slug($request->nombre);;
    	$articulo->save();
    	foreach ($request->atributo as $value) {
    		if ($value['value']) {
                if (!isset($value['lista'])) {
                    $articulo->atributos_articulo()->attach([$value['id'] => ['contenido' => $value['value']]]);
                }else{
                    $articulo->atributos_articulo()->attach([$value['id'] => ['contenido' => $value['value'].'|_|'.$value['lista']]]);
                }
    		}
    	}

	 	return redirect(route('ListarArticulosAdmin'))->with([
            'mensage' => 'Articulo creado con exito.',
            'type' => 'success',
            'title' => 'Articulos'
        ]);
    
    }

    public function editar($id){
    	$articulo=Articulo::find($id);

        $articulo->with('atributos_articulo','categoria')->get();
        
        
        $arbol=$this->ArbolDeCategoriasInvertido($articulo->categoria->id);
        $padre=end($arbol);
        
        $requeridos=Categoria::with('atributos_categorias')->where('id',$articulo->categoria->id)->get()->first();
        

        $opcionales=Atributo::where('categoria_id',$padre['id'])->get();

        $atributo_articulo2=$articulo->atributos_articulo;
        $requerido=false;
        $opcional=false;

        //Buscamos Todos los atributos requeridos con su contenido si este existe 
        foreach ($requeridos->atributos_categorias as $key => $value) {
            $seleccionado=false;
            foreach ($articulo->atributos_articulo as $key2 =>$value2) {
                if ($value->id == $value2->id) {
                    $seleccionado = $value2;
                    unset($articulo->atributos_articulo[$key2]);
                }
            }
            if ($seleccionado) {
                $requerido[]=$seleccionado;
            }else{
                $requerido[]=$value;
            }
        }

        foreach ($opcionales as $key => $value) {
            $seleccionado=false;
            $aceptar=true;
            //Para eliminar los requeridos de los opcionales

            foreach ($requeridos->atributos_categorias as $key2 =>$value2) {
                if ($value->id == $value2->id) {
                    $aceptar=false;
                    unset($opcionales[$key]);
                    unset($requeridos->atributos_categorias[$key2]);
                }
            }
            
            foreach ($articulo->atributos_articulo as $key2 =>$value2) {
                if ($value->id == $value2->id) {
                    $seleccionado = $value2;
                    unset($articulo->atributos_articulo[$key2]);
                }
            }

            if ($aceptar) {
                if ($seleccionado) {
                    $opcional[]=$seleccionado;
                }else{
                    $opcional[]=$value;
                }
            }
        }

    	$categorias=$this->ArbolDeCategoriasSelect();
    	return view('adminlte.articulos.editar',[
            'categoria' => $categorias,
    		'requeridos' => $requerido,
            'opcionales' => $opcional,
    		'articulo' => $articulo
    	]);
    }

    public function update($id, Request $request){
        $articulo=Articulo::find($id);
        $articulo->nombre=$request->nombre;
        $articulo->precio=$request->precio;
        $articulo->foto=$request->foto;
        $articulo->galeria=$request->galeria;
        $articulo->descripcion=$request->descripcion;
        $articulo->categoria_id=$request->categoria_id;
        $articulo->slug='NANO'.$articulo->id.'-'.Str::slug($request->nombre);
        $articulo->presupuesto=$request->presupuesto;
        $articulo->save();
        DB::table('atributo_articulo')->where('articulo_id', $id)->delete();
        foreach ($request->atributo as $value) {
            if ($value['value']) {
                if (isset($value['lista'])) {
                    $articulo->atributos_articulo()->attach([$value['id'] => ['contenido' => $value['value'].'|_|'.$value['lista']]]);
                }else{
                    $articulo->atributos_articulo()->attach([$value['id'] => ['contenido' => $value['value']]]);
                }
            }
        }
        return redirect(route('ListarArticulosAdmin'))->with([
            'mensage' => 'Articulo editado con exito.',
            'type' => 'success',
            'title' => 'Articulos'
        ]);
    
    }

    public function eliminar($id){
        $articulo=Articulo::destroy($id);
        return redirect(route('ListarArticulosAdmin'))->with([
            'mensage' => 'Articulo eliminado con exito.',
            'type' => 'success',
            'title' => 'Articulos'
        ]);
    }

    public function ArbolDeCategoriasSelect($padre_id = 0, $arbol_de_categorias = '', $espacio = ''){
        if (!is_array($arbol_de_categorias)){   
            $arbol_de_categorias = array();
        }
        $categoria=Categoria::where('padre_id',$padre_id)->where('active','1')->get();
        for ($i=0; $i < count($categoria); $i++) { 
            $arbol_de_categorias[] = array("id" => $categoria[$i]->id, "nombre" => $espacio.$categoria[$i]->nombre, "padre_id" => $categoria[$i]->padre_id);
            $arbol_de_categorias = $this->ArbolDeCategoriasSelect($categoria[$i]->id, $arbol_de_categorias,$espacio.'--- ');
        }
        return $arbol_de_categorias;
    }

    public function ArbolDeCategoriasInvertido($padre_id = 0, $arbol_de_categorias = ''){
        if (!is_array($arbol_de_categorias)){   
            $arbol_de_categorias = array();
        }
        $categoria=Categoria::where('id',$padre_id)->where('active','1')->get();
        for ($i=0; $i < count($categoria); $i++) { 
            $arbol_de_categorias[] = array("id" => $categoria[$i]->id, "nombre" => $categoria[$i]->nombre, "padre_id" => $categoria[$i]->padre_id);
            $arbol_de_categorias = $this->ArbolDeCategoriasInvertido($categoria[$i]->padre_id, $arbol_de_categorias);
        }
        return $arbol_de_categorias;
    }

}
