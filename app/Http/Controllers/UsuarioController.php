<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Presupuesto;
use App\Categoria;
use App\Articulo;
use App\Atributo;
use App\Pedido;
use App\User;
use Illuminate\Support\Str as Str;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailPedidoEstado;

class UsuarioController extends Controller
{
    public function index(){
    	$usuarios=User::all();
    	return view('adminlte.usuarios.index',[
    		'usuarios' => $usuarios
    	]);
    }

    public function ver($id){
    	$usuario=User::find($id);
    	$usuario->each(function($usuario){
    		$usuario->articulos_usuarios;
            $usuario->pedidos;
    	});

        $presupuestos=Presupuesto::where('user_id',$id)->get();
        $presupuestos->each(function($presupuestos){
            $presupuestos->articulo;
        });
    	return view('adminlte.usuarios.ver',[
            'usuario' => $usuario,
    		'presupuestos' => $presupuestos
    	]);
    }

    public function crear(Request $request){
    	return view('adminlte.usuarios.crear');
    }

    public function add(Request $request){
    	$usuario=new User;
    	$usuario->user=$request->user;
    	$usuario->name=$request->name;
    	$usuario->email=$request->email;
    	$usuario->telefono=$request->telefono;
    	$usuario->type=$request->type;
    	$usuario->password=bcrypt($request->password);
    	$usuario->save();
    	return redirect()->back()->with([
    		'mensage' => 'Creado con Exito!.',
			'type' => 'success',
			'title' => 'Usuarios'
    	]);
    }

    public function update($id,Request $request){
    	$usuario=User::find($id);
    	$usuario->user=$request->user;
    	$usuario->name=$request->name;
    	$usuario->email=$request->email;
    	$usuario->telefono=$request->telefono;
    	$usuario->type=$request->type;
    	if (isset($request->password)) {
    		$usuario->password=bcrypt($request->password);
    	}
    	
    	$usuario->save();
    	return redirect()->back()->with([
    		'mensage' => 'Actualizado con Exito!.',
			'type' => 'success',
			'title' => 'Usuarios'
    	]);
    }

    public function eliminar($id){
    	$usuario=User::destroy($id);
    	return redirect()->back()->with([
    		'mensage' => 'Eliminado con Exito!.',
			'type' => 'success',
			'title' => 'Usuarios'
    	]);
    }



}
