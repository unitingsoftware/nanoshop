<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Articulo;
use App\Categoria;
use App\Atributo;
use App\Pedido;
use App\User;
use App\Presupuesto;
use Illuminate\Support\Str as Str;
use DB;
use Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailNuevoPresupuesto;
use App\Mail\MailNuevaCompra;
use App\Mail\MailGarantiaSoporte;
use App\Mail\MailContacto;

class FrontController extends Controller{
    public function __construct(){
        if (!Session::has('carrito')){
            Session::put('carrito',array());
        }

    	if (!Session::has('comparar')){
    		Session::put('comparar',array());
    	} 
    }

    /*****************
	*  Tienda Online
	******************/

    public function index(){
    	return view('front.index',[
            'categorias' => $this->ArbolDeCategoriasMenu(),
            'carrito' => $this->CarritoCompleto(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador()
        ]);
    }

    public function categoria($id,$nombre){
        $categoria=Categoria::where('id',$id)->limit(1)->first();
        
        $filtros=Articulo::whereIn('categoria_id',$this->ArbolDeCategoriasArticulo($id))->get();
        $filtros->each(function($filtros){
            $filtros->atributos_articulo;
        });

        $atributos=[];
        foreach ($filtros as $articulo) {

            //Creamos los filtros e inicializamos en 0 sus atributos
            foreach ($articulo->atributos_articulo as $atributo) {
                if ($atributo AND $atributo->tipo == 'select') {

                    $fila=explode(',', $atributo->contenido);
                    if (!isset($atributos[$atributo->nombre])) {
                        $atributos[$atributo->nombre]=[
                            'nombre' => $atributo->nombre
                        ];
                    }

                    foreach ($fila as $value) {
                        if (!isset($atributos[$atributo->nombre]['contenido'][$value])) {
                            $atributos[$atributo->nombre]['contenido'][$value]=[
                                'nombre' => $value,
                                'cantidad' => 0
                            ];
                        }
                        
                    }
                }
            }

            foreach ($articulo->atributos_articulo as $atributo) {
                //agregamos cantidades a los atributos
                if (isset($atributos[$atributo->nombre]['contenido'][$atributo->pivot->contenido])) {
                    $resultado=$atributos[$atributo->nombre]['contenido'][$atributo->pivot->contenido]['cantidad']+1;
                    $atributos[$atributo->nombre]['contenido'][$atributo->pivot->contenido]['cantidad']=$resultado;
                }
            }
        }   
        if (isset($_GET['filtro'])) {
            $filtro_actual=$_GET['filtro'];
        }else{
            $filtro_actual=[];
        }

        if (count($filtro_actual)) {
            $articulos=Articulo::whereIn('categoria_id',$this->ArbolDeCategoriasArticulo($id))
                ->with('atributos_articulo')->with('categoria')
                ->whereHas('atributos_articulo', function($query) use($filtro_actual){
                    $query->whereIn('atributo_articulo.contenido',$filtro_actual);
                })
            ->paginate(9);
        }else{
            $articulos=Articulo::whereIn('categoria_id',$this->ArbolDeCategoriasArticulo($id))->paginate(9);
            $articulos->each(function($articulos){
                $articulos->categoria;
            });
        }

        return view('front.tienda',[
            'carrito' => $this->CarritoCompleto(),
            'titulo_categoria' => $categoria->nombre,
            'categoria' => $categoria,
            'menu_categoria' => $this->ArbolDeCategoriasTienda($categoria->id),
            'categorias' => $this->ArbolDeCategoriasMenu(),
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'articulos' => $articulos,
            'filtros' => $atributos,
            'filtro_actual' => $filtro_actual
        ]);
    }

    public function tienda(){
        $articulos=Articulo::paginate(9);
        $articulos->each(function($articulos){
            $articulos->categoria;
        });
        return view('front.tienda',[
            'carrito' => $this->CarritoCompleto(),
            'titulo_categoria' => 'Categorias',
            'categoria' => false,
            'menu_categoria' => $this->ArbolDeCategoriasTienda(),
            'categorias' => $this->ArbolDeCategoriasMenu(),
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'articulos' => $articulos
        ]);
    }

    public function BuscadorArticulo(Request $request){
        if ($request->categoria == 0) {
            $articulos=Articulo::where('nombre','LIKE','%'.$request->s.'%')->paginate(9);
            $articulos->each(function($articulos){
                $articulos->categoria;
            });
            return view('front.tienda',[
                'carrito' => $this->CarritoCompleto(),
                'titulo_categoria' => 'Categorias',
                'categoria' => false,
                'menu_categoria' => $this->ArbolDeCategoriasTienda(),
                'categorias' => $this->ArbolDeCategoriasMenu(),
                'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
                'comparar_count' => count($this->ComparacionCompleto()),
                'articulos' => $articulos
            ]);
        }else{

            $categoria=Categoria::where('id',$request->categoria)->limit(1)->first();

            $articulos=Articulo::where('nombre','LIKE','%'.$request->s.'%')->whereIn('categoria_id',$this->ArbolDeCategoriasArticulo($request->categoria))->paginate(9);
            $articulos->each(function($articulos){
                $articulos->categoria;
            });

            return view('front.tienda',[
                'carrito' => $this->CarritoCompleto(),
                'titulo_categoria' => $categoria->nombre,
                'categoria' => $categoria,
                'menu_categoria' => $this->ArbolDeCategoriasTienda($categoria->id),
                'categorias' => $this->ArbolDeCategoriasMenu(),
                'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
                'comparar_count' => count($this->ComparacionCompleto()),
                'articulos' => $articulos
            ]);
        }
    }

    public function articulo(Articulo $articulo){
        $articulo->each(function($articulo){
            $articulo->categoria;
            $articulo->atributos_articulo;
        });
        return view('front.articulo',[
            'carrito' => $this->CarritoCompleto(),
            'titulo_categoria' => 'Categorias',
            'categoria' => false,
            'menu_categoria' => $this->ArbolDeCategoriasTienda(),
            'categorias' => $this->ArbolDeCategoriasMenu(),
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'articulo' => $articulo
        ]);
    }

    public function PresupuestoArticulo(Articulo $articulo,Request $request){
        $presupuesto=new Presupuesto();
        $presupuesto->name=$request->name;
        $presupuesto->email=$request->email;
        $presupuesto->telefono=$request->telefono;
        $presupuesto->cantidad=$request->cantidad;
        $presupuesto->empresa=$request->empresa;
        $presupuesto->articulo_id=$articulo->id;
        $presupuesto->estado='Pendiente';

        if (auth()->user()) {
            $presupuesto->user_id=auth()->user()->id;
        }
        $presupuesto->save();

        $presupuesto->each(function($presupuesto){
            $presupuesto->articulo;
        });
        Mail::to(env('MAIL_TO'))->send(new MailNuevoPresupuesto($presupuesto));
        
        return redirect()->back();
    }

    /***************
    *   Comparar
    ****************/

    public function VerComparacion(){
        if (auth()->user()) {
            $user=User::where('id',auth()->user()->id)->get()->first();
            $articulos=$user->articulos_usuarios()->where('tipo','comparar')->get();
            $articulos->each(function ($articulos){
                $articulos->atributos_articulo;
                $articulos->categoria;
            });
        }else{
            $session_comparar=null;
            $articulos=session()->get('comparar');
            foreach ($articulos as $value) {
                if ($value) {
                    $session_comparar.=$value->id;
                }
            }
            $articulos=Articulo::whereIn('id',[$session_comparar]);
            $articulos->each(function ($articulos){
                $articulos->atributos_articulo;
                $articulos->categoria;
            });
        }

        return view('front.comparacion',[
            'categoria' => false,
            'carrito' => $this->CarritoCompleto(),
            'categorias' => $this->ArbolDeCategoriasMenu(),
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'articulos' => $this->ComparacionCompleto()
        ]);

    }

    public function AgregarArticuloComparacion(Request $request,Articulo $articulo){
        if (auth()->user()) {
            //Si es un usuario Registrado
            $articulo_comparar=$articulo->articulos_usuarios()->
                    where('tipo','comparar')->
                    where('articulo_id',$articulo->id)->
                    where('user_id',auth()->user()->id)->
                    first();
            if ($articulo_comparar) {
                $articulo->articulos_usuarios()->detach(auth()->user()->id);
                $articulo->articulos_usuarios()->attach(auth()->user()->id,['cantidad' => 0, 'tipo' => 'comparar']);
            }else{
                $articulo->articulos_usuarios()->attach(auth()->user()->id,['cantidad' => 0, 'tipo' => 'comparar']);
            }
        }else{
            //Si es un usuario Sin  Registro
            $comparar=session()->get('comparar');
            $comparar[$articulo->slug]=$articulo;
            session()->put('comparar',$comparar);
        }
        return redirect()->back();
    }

    public function ComparacionCompleto(){
        if (auth()->user()) {
            $user=User::find(auth()->user()->id);
            if (Session::has('comparar')){
                foreach (session()->get('comparar') as $key => $value) {
                    if ($value) {
                        $verificar=$user->articulos_usuarios()->where('articulo_id',$value->id)->first();
                        if (!$verificar) {
                            $user->articulos_usuarios()->attach($value->id,['cantidad' => 0, 'tipo' => 'comparar']);
                        }
                    }
                }
            }
            $comparar=$user->articulos_usuarios()->where('tipo','comparar')->get();
            session()->pull('comparar');
        }else{
            $comparar=session()->get('comparar');
        }
        return $comparar;

    }


    public function EliminarComparacion(Request $request,Articulo $articulo){
        if (auth()->user()) {
            $user=User::find(auth()->user()->id);
            $user->articulos_usuarios()->detach($articulo->id);
        }else{
            $comparar=session()->get('comparar');
            unset($comparar[$articulo->slug]);
            session()->put('comparar',$comparar);
        }


        return redirect()->back();
    }

    public function EliminarTodaComparacion(Request $request,Articulo $articulo){
        if (auth()->user()) {
            $user=User::find(auth()->user()->id);
            $user->articulos_usuarios()->detach($articulo->id);
        }else{
            $comparar=session()->get('comparar');
            unset($comparar);
            session()->put('comparar',$comparar);
        }


        return redirect()->back();
    }


    /***************
    *   Carrito
    ****************/

    public function CarritoCompleto(){
        if (auth()->user()) {
            $user=User::find(auth()->user()->id);
            foreach (session()->get('carrito') as $key => $value) {
                if ($value) {
                    $verificar=$user->articulos_usuarios()->where('articulo_id',$value->id)->first();
                    if (!$verificar) {
                        $user->articulos_usuarios()->attach($value->id,['cantidad' => $value->cantidad, 'tipo' => 'carrito']);
                    }
                }
            }
            $carrito=$user->articulos_usuarios()->where('tipo','carrito')->get();
            session()->pull('carrito');
        }else{
            $carrito=session()->get('carrito');
        }
        return $carrito;

    }

    public function VerCarrito(){
        if (auth()->user()) {
            $user=User::where('id',auth()->user()->id)->get()->first();
            $articulos=$user->articulos_usuarios()->where('tipo','carrito')->get();
        }else{
            $articulos=session()->get('carrito');
        }
        return view('front.carrito',[
            'carrito' => $this->CarritoCompleto(),
            'categrias' => $this->ArbolDeCategoriasMenu(),
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'articuloso' => $articulos
        ]);

    }

    public function AgregarCarrito(Request $request,Articulo $articulo){
        if (isset($request->cantidad)) {
            $cantidad=$request->cantidad;
        }else{
            $cantidad=1;
        }
        if (auth()->user()) {
            //Si es un usuario Registrado
            $articulo_carrito=$articulo->articulos_usuarios()->
                    where('tipo','carrito')->
                    where('articulo_id',$articulo->id)->
                    where('user_id',auth()->user()->id)->
                    first();
            if ($articulo_carrito) {
                $cantidad=$articulo_carrito->pivot->cantidad+$cantidad;
                $articulo->articulos_usuarios()->detach(auth()->user()->id);
                $articulo->articulos_usuarios()->attach(auth()->user()->id,['cantidad' => $cantidad, 'tipo' => 'carrito']);
            }else{
                $articulo->articulos_usuarios()->attach(auth()->user()->id,['cantidad' => $cantidad, 'tipo' => 'carrito']);
            }
        }else{
            //Si es un usuario Sin  Registro
            $carrito=session()->get('carrito');
            $articulo->cantidad=$cantidad;
            $carrito[$articulo->slug]=$articulo;
            session()->put('carrito',$carrito);
        }
        return redirect()->back();
    }

    public function ActualizarCarrito(Request $request){
        $articulo=Articulo::find($_GET['id']);
        $total=0;
        if (auth()->user()) {
            //Si es un usuario Registrado
            $articulo->articulos_usuarios()->detach(auth()->user()->id);
            $articulo->articulos_usuarios()->attach(auth()->user()->id,['cantidad' => $_GET['cantidad'], 'tipo' => 'carrito']);
            $user=User::where('id',auth()->user()->id)->get()->first();
            $carrito=$user->articulos_usuarios()->where('tipo','carrito')->get();
            foreach ($carrito as $key => $value) {
                $total=($value->precio*$value->pivot->cantidad)+$total;
            }
        }else{
            $carrito=session()->get('carrito');
            unset($carrito[$articulo->slug]);
            $articulo->cantidad=$_GET['cantidad'];
            $carrito[$articulo->slug]=$articulo;
            session()->put('carrito',$carrito);
            foreach ($carrito as $key => $value) {
                $total=($value->precio*$value->cantidad)+$total;
            }
        }

        return response()->json([
            $total
        ]);
    }

    public function EliminarCarrito(Request $request,Articulo $articulo){
        if (auth()->user()) {
            $user=User::find(auth()->user()->id);
            $user->articulos_usuarios()->detach($articulo->id);
        }else{
            $carrito=session()->get('carrito');
            unset($carrito[$articulo->slug]);
            session()->put('carrito',$carrito);
        }


        return redirect()->back();
    }

    public function VaciarCarrito(Request $request,Articulo $articulo){
        session()->forget('carrito',$articulo->slug);
        return redirect()->back();
    }

    /**************************
    *   Usuarios
    ***************************/
    public function login(){
        if (auth()->user()) {
            return redirect(route('TiendaMiCuentaFront'));
        }
        return view('front.usuarios.login',[
            'carrito' => $this->CarritoCompleto(),
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'categorias' => $this->ArbolDeCategoriasMenu()
        ]);
    }

    public function MiCuenta(){
        return view('front.usuarios.cuenta',[
            'carrito' => $this->CarritoCompleto(),
            'categorias' => $this->ArbolDeCategoriasMenu(),
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'usuario' => auth()->user()
        ]);
    }

    /**************************
    *   Pedidos
    ***************************/

    public function GenerarPedido(){
        $carrito=session()->get('carrito');
        return view('front.pedido',[
            'carrito' => $this->CarritoCompleto(),
            'categorias' => $this->ArbolDeCategoriasMenu(),
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'articulos' => $carrito
        ]);
    }
    public function EjecutarPedido(){
        $carrito=$this->CarritoCompleto();
        $user=User::find(auth()->user()->id);
        $pedido=new Pedido;
        $pedido->user_id=auth()->user()->id;
        $pedido->articulos=0;
        $pedido->save();
        foreach ($carrito as $value) {
            if ($value) {
                $pedido->articulos_pedido()->attach([
                    $value->id => [
                        'precio' => $value->precio, 
                        'cantidad' => $value->pivot->cantidad
                    ]
                ]);
                $pedido->total=($value->precio*$value->pivot->cantidad)+$pedido->total;
                $pedido->articulos=$value->pivot->cantidad+$pedido->articulos;
                $user->articulos_usuarios()->detach($value->id);
            }
        }
        $pedido->save();
        Mail::to(env('MAIL_TO'))->send(new MailNuevaCompra($pedido));
        return redirect(route('TiendaPedidoVerFront',$pedido->id));
    }

    public function VerPedido($id){
        $pedido=Pedido::where('id',$id)->where('user_id',auth()->user()->id)->first();
        $pedido->each(function($pedido){
            $pedido->articulos_pedido;
        });
        return view('front.pedido.ver',[
            'carrito' => $this->CarritoCompleto(),
            'pedido' => $pedido,
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'categorias' => $this->ArbolDeCategoriasMenu()
        ]);
    }

    public function ListarPedido(){
        $pedido=Pedido::where('user_id',auth()->user()->id)->get();
        return view('front.pedido.listar',[
            'carrito' => $this->CarritoCompleto(),
            'pedidos' => $pedido,
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'categorias' => $this->ArbolDeCategoriasMenu()
        ]);
    }

    /**************************
    *   Paginas Extras
    ***************************/
    public function QuienesSomosFront(){
        return view('front.quienessomos',[
            'carrito' => $this->CarritoCompleto(),
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'categorias' => $this->ArbolDeCategoriasMenu()
        ]);
    }

    public function GarantiaSoporteFront(){
        return view('front.garantiaysoporte',[
            'carrito' => $this->CarritoCompleto(),
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'categorias' => $this->ArbolDeCategoriasMenu()
        ]);
    }

    public function ContactoFront(){
        return view('front.contacto',[
            'carrito' => $this->CarritoCompleto(),
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'categorias' => $this->ArbolDeCategoriasMenu()
        ]);
    }

    public function ClientesFront(){
        return view('front.clientes',[
            'carrito' => $this->CarritoCompleto(),
            'categorias_buscador' => $this->ArbolDeCategoriasBuscador(),
            'comparar_count' => count($this->ComparacionCompleto()),
            'categorias' => $this->ArbolDeCategoriasMenu()
        ]);
    }

    /*********************
    *   Enviar Correos
    **********************/

    public function GarantiaSoporteCorreoFront(Request $request){


        Mail::to(env('MAIL_TO'))->send(new MailGarantiaSoporte($request->all()));
        
        return redirect()->back();
    }

    public function ContactoCorreoFront(Request $request){
    //    dd($request->all());
        Mail::to(env('MAIL_TO'))->send(new MailContacto($request->all()));
        
        return redirect()->back();
    }

    /**************************
    *   Arboles de Categorias
    ***************************/

    public function HayHijo($padre_id){
        return Categoria::where('padre_id',$padre_id)->count();
    }

    public function ArbolDeCategoriasBuscador($padre_id = 0, $arbol_de_categorias = '', $espacio = ''){
        if (!is_array($arbol_de_categorias)){   
            $arbol_de_categorias = array();
        }
        $categoria=Categoria::where('padre_id',$padre_id)->get();
        for ($i=0; $i < count($categoria); $i++) { 
            

            $arbol_de_categorias[] = array("id" => $categoria[$i]->id, "nombre" => $espacio.$categoria[$i]->nombre, "padre_id" => $categoria[$i]->padre_id);
            $arbol_de_categorias = $this->ArbolDeCategoriasBuscador($categoria[$i]->id, $arbol_de_categorias,$espacio.'&nbsp;&nbsp;&nbsp;&nbsp;');
        

        }
        return $arbol_de_categorias;
    }

    public function ArbolDeCategoriasArticulo($padre_id = 0,$arbol_de_categorias = ''){
    	if (!is_array($arbol_de_categorias)){   
	        $arbol_de_categorias= array();
	        $arbol_de_categorias[]= $padre_id;
        }

        $categoria=Categoria::where('padre_id',$padre_id)->get();
        for ($i=0; $i < count($categoria); $i++) { 
        	$arbol_de_categorias[]= $categoria[$i]->id;
        	$arbol_de_categorias = $this->ArbolDeCategoriasArticulo($categoria[$i]->id, $arbol_de_categorias);
        }
        return $arbol_de_categorias;
    }

    public function ArbolDeCategoriasMenu($padre_id = 0, $arbol_de_categorias = '', $nivel = 0){
        $nivel++;
        if ($nivel > 3) {
        	return null;
        }else{
	        if (!is_array($arbol_de_categorias)){   
	            $arbol_de_categorias = array();
	        }
	        $categoria=Categoria::where('padre_id',$padre_id)->get();
        	
        	switch ($nivel) {
        		case 1:
	        		$arbol_de_categorias[] = '<ul class="menu">';
        		break;
        		case 2:
	        		$arbol_de_categorias[] = '';
        		break;
        		default:
	        		$arbol_de_categorias[] = '<ul>';
        		break;
        	}
	        for ($i=0; $i < count($categoria); $i++) { 
	        	switch ($nivel) {
	        		case 1:
	        			$arbol_de_categorias[] = '<li>';
						
	        			if ($this->HayHijo($categoria[$i]->id)) {
							$arbol_de_categorias[] = '<a href="'.route('TiendaCategoriaFront',array($categoria[$i]->id,$categoria[$i]->nombre)).'" title="" class="dropdown">';
							$arbol_de_categorias[] = '<span class="menu-title">';
							$arbol_de_categorias[] = $categoria[$i]->nombre;
							$arbol_de_categorias[] = '</span>';
							$arbol_de_categorias[] = '</a>';
							$arbol_de_categorias[] = '<div class="drop-menu">';
							$arbol_de_categorias = $this->ArbolDeCategoriasMenu($categoria[$i]->id, $arbol_de_categorias, $nivel);
							$arbol_de_categorias[] = '</div>';
	        			}else{
	        				$arbol_de_categorias[] = '<a href="'.route('TiendaCategoriaFront',array($categoria[$i]->id,$categoria[$i]->nombre)).'" title="">';
							$arbol_de_categorias[] = '<span class="menu-title">';
							$arbol_de_categorias[] = $categoria[$i]->nombre;
							$arbol_de_categorias[] = '</span>';
							$arbol_de_categorias[] = '</a>';
	        			}


						$arbol_de_categorias[] = '</li>';
        			break;
	        		case 2:
						$arbol_de_categorias[] = '<div class="one-third">';
						if ($this->HayHijo($categoria[$i]->id)) {
							$arbol_de_categorias[] = '<div class="cat-title">';
							$arbol_de_categorias[] = $categoria[$i]->nombre;
							$arbol_de_categorias[] = '</div>';
		
		            		$arbol_de_categorias = $this->ArbolDeCategoriasMenu($categoria[$i]->id, $arbol_de_categorias, $nivel);

	    					$arbol_de_categorias[] = '<div class="show">';
							$arbol_de_categorias[] = '<a href="'.route('TiendaCategoriaFront',array($categoria[$i]->id,$categoria[$i]->nombre)).'" title="">Ver Todos</a>';
							$arbol_de_categorias[] = '</div>';
						}else{
							$arbol_de_categorias[] = '<div class="cat-title">';
							$arbol_de_categorias[] = '<a href="'.route('TiendaCategoriaFront',array($categoria[$i]->id,$categoria[$i]->nombre)).'" title="">'.$categoria[$i]->nombre.'</a>';
							$arbol_de_categorias[] = '</div>';
						}

						$arbol_de_categorias[] = '</div>';
        			break;
	        		case 3:
	        			$arbol_de_categorias[] = '<li>';
						$arbol_de_categorias[] = '<a href="'.route('TiendaCategoriaFront',array($categoria[$i]->id,$categoria[$i]->nombre)).'" title="">'.$categoria[$i]->nombre.'</a>';
						$arbol_de_categorias[] = '</li>';
        			break;
	        	}
	        }
	        switch ($nivel) {
        		case 2:
	        		$arbol_de_categorias[] = '';
        		break;
        		default:
	        		$arbol_de_categorias[] = '</ul>';
        		break;
        	}
	        return $arbol_de_categorias;
        }
    }

    public function ArbolDeCategoriasTienda($padre_id = 0, $arbol_de_categorias = '', $nivel = 0){
        $nivel++;
        if ($nivel > 2) {
        	return null;
        }else{
	        if (!is_array($arbol_de_categorias)){   
	            $arbol_de_categorias = array();
	        }
	        $categoria=Categoria::where('padre_id',$padre_id)->get();
        	
        	switch ($nivel) {
        		case 1:
	        		$arbol_de_categorias[] = '<ul class="cat-list style1 widget-content">';
        		break;
        		default:
	        		$arbol_de_categorias[] = '<ul class="cat-child">';
        		break;
        	}
	        for ($i=0; $i < count($categoria); $i++) { 
	        	switch ($nivel) {
	        		case 1:
	        			$arbol_de_categorias[] = '<li>';
						
	        			if ($this->HayHijo($categoria[$i]->id)) {
							$arbol_de_categorias[] = '<span>'.$categoria[$i]->nombre.'</span>';
							$arbol_de_categorias = $this->ArbolDeCategoriasTienda($categoria[$i]->id, $arbol_de_categorias, $nivel);
							$arbol_de_categorias[] = '<ul class="cat-child">';
							$arbol_de_categorias[] = '<li>';
							$arbol_de_categorias[] = '<a href="'.route('TiendaCategoriaFront',array($categoria[$i]->id,$categoria[$i]->nombre)).'" title="">Ver Todos</a>';
							$arbol_de_categorias[] = '</li>';
							$arbol_de_categorias[] = '</ul>';
	        			}else{
	        				$arbol_de_categorias[] = '<span class="list_sin_hijos">';
							$arbol_de_categorias[] = '<a href="'.route('TiendaCategoriaFront',array($categoria[$i]->id,$categoria[$i]->nombre)).'" title="">'.$categoria[$i]->nombre.'</a>';
							$arbol_de_categorias[] = '</span>';
	        			}

						$arbol_de_categorias[] = '</li>';
        			break;
	        		case 2:
						$arbol_de_categorias[] = '<li>';
						$arbol_de_categorias[] = '<a href="'.route('TiendaCategoriaFront',array($categoria[$i]->id,$categoria[$i]->nombre)).'" title="">'.$categoria[$i]->nombre.'</a>';
						$arbol_de_categorias[] = '</li>';
        			break;
        		}
	        }
    		$arbol_de_categorias[] = '</ul>';
        		
	        return $arbol_de_categorias;
        }
    }
}
