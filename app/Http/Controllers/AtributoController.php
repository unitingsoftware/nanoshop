<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Atributo;
use DB;

class AtributoController extends Controller
{
    public function index($categoria){
    	$arbol=$this->ArbolDeCategoriasInvertido($categoria);
        $padre=end($arbol);
        $requeridos=Categoria::where('id',$categoria)->get()->first();
        $requeridos->each(function($requeridos,$padre){
            $requeridos->atributos_categorias;
        });
        $opcionales=Atributo::where('categoria_id',$padre['id'])->get();


    	return view('adminlte.atributos.index',[
    		'requeridos'=> $requeridos->atributos_categorias,
            'categoria'=> $requeridos,
            'opcionales'=> $opcionales,
    		'padre'=> $padre
    	]);
    }

    public function crear($categoria){
    	$padre=$this->ArbolDeCategoriasInvertido($categoria);
    	$actual=Categoria::find($categoria);
    	
    	return view('adminlte.atributos.crear',[
    		'padre'=> end($padre),
    		'actual'=> $actual
    	]);
    }

    public function add($categoria, Request $request){
        $arbol=$this->ArbolDeCategoriasInvertido($categoria);
    	$padre=end($arbol);

        //Guardamos Atributo en la categoria padre (Base)
        $atributo= new Atributo();
        $atributo->tipo=$request->tipo;
        $atributo->nombre=$request->nombre;
        $atributo->contenido=$request->contenido;
        $atributo->categoria_id=$padre['id'];
        $atributo->save();
        
        //Hacemos Atributo requerido en la categoria actual
        $atributo->atributos_categorias()->sync($categoria);
        
        return redirect(route('ListarAtributoCategoriaAdmin',$categoria))->with([
            'mensage' => 'Atributo guardado.',
            'type' => 'success',
            'title' => 'Atributo'
        ]);

    }

    public function editar($categoria,$atributo){
        
        $categoria=Categoria::find($categoria);
        $padre=$this->ArbolDeCategoriasInvertido($categoria);
        $actual=Atributo::find($atributo);
        
        return view('adminlte.atributos.editar',[
            'padre'=> end($padre),
            'actual'=> $actual,
            'categoria'=> $categoria
        ]);
    }

    public function update($categoria,$atributo,Request $request){
        $atributo=Atributo::find($atributo);
        $atributo->tipo=$request->tipo;
        $atributo->nombre=$request->nombre;
        $atributo->contenido=$request->contenido;
        $atributo->save();

        return redirect(route('ListarAtributoCategoriaAdmin',$categoria))->with([
            'mensage' => 'Atributo Editado con Exito.',
            'type' => 'success',
            'title' => 'Atributo'
        ]);
    }

    public function eliminar($categoria,$atributo){
        $atributo=Atributo::destroy($atributo);
        
        return redirect(route('ListarAtributoCategoriaAdmin',$categoria))->with([
            'mensage' => 'Atributo Eliminado con Exito.',
            'type' => 'success',
            'title' => 'Atributo'
        ]);
        
    }

    public function requerir($categoria,$atributo){
        //Hacemos Atributo requerido en la categoria actual

        $atributo=Atributo::find($atributo);
        $atributo->atributos_categorias()->attach($categoria);
        
        return redirect(route('ListarAtributoCategoriaAdmin',$categoria))->with([
            'mensage' => 'Atributo Añadido a Requeridos.',
            'type' => 'success',
            'title' => 'Atributo'
        ]);
        
    }

    public function retirar($categoria,$atributo){
        $atributo=Atributo::find($atributo);
        $atributo->atributos_categorias()->detach($categoria,$atributo);
        return redirect(route('ListarAtributoCategoriaAdmin',$categoria))->with([
            'mensage' => 'Atributo Retirado de Requeridos.',
            'type' => 'success',
            'title' => 'Atributo'
        ]);
    }

    public function ajaxArticulo($id = 0){
        $arbol=$this->ArbolDeCategoriasInvertido($id);
        $padre=end($arbol);
        $requeridos=Categoria::where('id',$id)->get()->first();
        $requeridos->each(function($requeridos,$padre){
            $requeridos->atributos_categorias;
        });
        $opcionales=Atributo::where('categoria_id',$padre['id'])->get();
        $i=0;
        echo '
            <div class="box-header with-border">
                <h3 class="box-title">Atributos Requeridos</h3>
            </div>
            <div class="box-body">
        ';
        $seleccionados=[];
        foreach ($requeridos->atributos_categorias as $key => $requerido) {
            switch ($requerido->tipo) {
                case 'select':
                    ?>
                        <div class="form-group">
                            <label for="nombre"><?php echo $requerido->nombre; ?></label>
                            <select required class="form-control" name="atributo[<?php echo $i; ?>][value]">
                                <option value="">Seleccionar</option>
                                <?php
                                    $lista=explode(",", $requerido->contenido);
                                    foreach ($lista as  $elemento) {
                                        echo '<option>'.$elemento.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    <?php                    
                break;
                case 'number_list':
                    ?>
                        <div class="form-group">
                            <label for="nombre" style="width: 100%;"><?php echo $requerido->nombre; ?></label>
                            <div class="col-md-6" style="padding-left: 0px;">
                                <input  type="number" class="form-control" name="atributo[<?php echo $i; ?>][value]" required="required">
                            </div>
                            <div class="col-md-6" style="padding-right: 0px;">
                                <select  class="form-control" name="atributo[<?php echo $i; ?>][lista]" required="required">
                                    <option value="">Seleccionar</option>
                                    <?php
                                        $lista=explode(",", $requerido->contenido);
                                        foreach ($lista as  $elemento) {
                                            echo '<option>'.$elemento.'</option>';
                                        }
                                    ?>
                                </select>
                                <br>
                            </div>
                        </div>
                    <?php
                break;
                case 'text':
                    ?>
                        <div class="form-group">
                            <label for="nombre"><?php echo $requerido->nombre; ?></label>
                            <input required type="text" class="form-control" name="atributo[<?php echo $i; ?>][value]">
                        </div>
                    <?php                    
                break;
                case 'number':
                    ?>
                        <div class="form-group">
                            <label for="nombre"><?php echo $requerido->nombre; ?></label>
                            <input required type="number" class="form-control" name="atributo[<?php echo $i; ?>][value]">
                        </div>
                    <?php                    
                break;
                
                default:
                    # code...
                break;
            }
            $seleccionados[]=$requerido->id;
            if ($requerido->tipo != 'number_list') {
                echo '<input type="hidden" class="form-control" name="atributo['.$i.'][lista]" value="">';
            }
            echo '<input type="hidden" class="form-control" name="atributo['.$i.'][id]" value="'.$requerido->id.'">';
            $i++;
        }
        echo '</div>';

        echo '
            <div class="box-header with-border">
                <h3 class="box-title">Atributos Opcionales</h3>
            </div>
            <div class="box-body">
        ';

        foreach ($opcionales as $key => $opcional) {
            if(!in_array($opcional->id, $seleccionados)){
                switch ($opcional->tipo) {
                    case 'select':
                        ?>
                            <div class="form-group">
                                <label for="nombre"><?php echo $opcional->nombre; ?></label>
                                <select class="form-control" name="atributo[<?php echo $i ; ?>][value]">
                                    <option value="">Seleccionar</option>
                                    <?php
                                        $lista=explode(",", $opcional->contenido);
                                        foreach ($lista as  $elemento) {
                                            echo '<option>'.$elemento.'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        <?php                    
                    break;
                    case 'number_list':
                        ?>
                            <div class="form-group">
                                <label for="nombre" style="width: 100%;"><?php echo $opcional->nombre; ?></label>
                                <div class="col-md-6" style="padding-left: 0px;">
                                    <input type="number" class="form-control" name="atributo[<?php echo $i; ?>][value]">
                                </div>
                                <div class="col-md-6" style="padding-right: 0px;">
                                    <select class="form-control" name="atributo[<?php echo $i ; ?>][lista]">
                                        <option value="">Seleccionar</option>
                                        <?php
                                            $lista=explode(",", $opcional->contenido);
                                            foreach ($lista as  $elemento) {
                                                echo '<option>'.$elemento.'</option>';
                                            }
                                        ?>
                                    </select>
                                <br><br>
                                </div>
                            </div>
                        <?php
                    break;
                    case 'text':
                        ?>
                            <div class="form-group">
                                <label for="nombre"><?php echo $opcional->nombre; ?></label>
                                <input type="text" class="form-control" name="atributo[<?php echo $i ; ?>][value]">
                            </div>
                        <?php                    
                    break;
                    case 'number':
                        ?>
                            <div class="form-group">
                                <label for="nombre"><?php echo $opcional->nombre; ?></label>
                                <input type="number" class="form-control" name="atributo[<?php echo $i ; ?>][value]">
                            </div>
                        <?php                    
                    break;
                    
                    default:
                        # code...
                    break;

                }
                if ($opcional->tipo != 'number_list') {
                    echo '<input type="hidden" class="form-control" name="atributo['.$i.'][lista]" value="">';
                }
                echo '<input type="hidden" class="form-control" name="atributo['.$i.'][id]" value="'.$opcional->id.'">';
                $i++;
            }
        }
        echo '</div>';


    }

    public function ArbolDeCategorias($padre_id = 0, $arbol_de_categorias = ''){
        if (!is_array($arbol_de_categorias)){   
            $arbol_de_categorias = array();
        }
        $categoria=Categoria::where('padre_id',$padre_id)->get();
        for ($i=0; $i < count($categoria); $i++) { 
            $arbol_de_categorias[] = array("id" => $categoria[$i]->id, "nombre" => $categoria[$i]->nombre, "padre_id" => $categoria[$i]->padre_id);
            $arbol_de_categorias = $this->ArbolDeCategorias($categoria[$i]->id, $arbol_de_categorias);
        }
        return $arbol_de_categorias;
    }


    public function ArbolDeCategoriasInvertido($padre_id = 0, $arbol_de_categorias = ''){
        if (!is_array($arbol_de_categorias)){   
            $arbol_de_categorias = array();
        }
        $categoria=Categoria::where('id',$padre_id)->get();
        for ($i=0; $i < count($categoria); $i++) { 
            $arbol_de_categorias[] = array("id" => $categoria[$i]->id, "nombre" => $categoria[$i]->nombre, "padre_id" => $categoria[$i]->padre_id);
            $arbol_de_categorias = $this->ArbolDeCategoriasInvertido($categoria[$i]->padre_id, $arbol_de_categorias);
        }
        return $arbol_de_categorias;
    }
}
