<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Articulo;
use App\Categoria;
use App\Atributo;
use App\Pedido;
use App\User;
use Illuminate\Support\Str as Str;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailPedidoEstado;

class PedidoController extends Controller{

    public function index($estado = null){
    	if ($estado == null) {
    		$pedidos=Pedido::get();
    	}else{
    		$pedidos=Pedido::where('estado',$estado)->get();
    	}
    	$pedidos->each(function($pedidos){
    		$pedidos->user;
    	});
    	return view('adminlte.pedidos.index',[
    		'pedidos' => $pedidos
    	]);
    }

    public function VerPedido($id){        
    	$pedido=Pedido::where('id',$id)->get();
    	$pedido->each(function($pedido){
            $pedido->articulos_pedido;
            $pedido->user;
        });
    	return view('adminlte.pedidos.ver',[
    		'pedido' => $pedido[0]
    	]);
    }

    public function CambioEstadoPedido($id, Request $request){
    	$pedido=Pedido::find($id);
    	$pedido->estado=$request->estado;
    	$pedido->save();
    	$pedido->each(function($pedido){
            $pedido->user;
        });
    	Mail::to($pedido->user->email)->send(new MailPedidoEstado($pedido));
    	return redirect()->back()->with([
			'mensage' => 'Estado Actualizado.',
			'type' => 'success',
			'title' => 'Pedidos'
		]);
    }
}
