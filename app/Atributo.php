<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atributo extends Model
{
    protected $table = 'atributos';

    public function categoria(){
    	return $this->belongsTo('App\Categoria');
    }

    public function atributos_categorias(){
    	return $this->belongsToMany('App\Categoria')->withTimestamps()->withPivot('id');
	}

    public function atributo_articulo(){
    	return $this->belongsToMany('App\Articulo')->withTimestamps()->withPivot('id');
	}
}
