<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user', 'password', 'email', 'telefono',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function articulos_usuarios(){
        return $this->belongsToMany('App\Articulo','articulos_usuarios')->withTimestamps()->withPivot('tipo','cantidad');
    }

    public function pedidos(){
        return $this->hasMany('App\Pedido');
    }

    public function presupuestos(){
        return $this->hasMany('App\Presupuesto');
    }
}
