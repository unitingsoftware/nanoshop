<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presupuesto extends Model
{
    protected $table = 'presupuestos';

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function articulo(){
    	return $this->belongsTo('App\Articulo');
    }
}
